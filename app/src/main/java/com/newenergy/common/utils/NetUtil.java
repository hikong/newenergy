package com.newenergy.common.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.net.URLDecoder;

public class NetUtil {

    //wap网络汇总
    public static final String CTWAP = "ctwap";
    public static final String CMWAP = "cmwap";
    public static final String WAP_3G = "3gwap";
    public static final String UNIWAP = "uniwap";


    /**
     * 判断当前网络是否是wifi
     *
     * @param context
     * @return
     */
    public static boolean isWifi(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetInfo != null && activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 是否是wifi或4g情况
     *
     * @param context
     * @return
     */
    public static boolean isWifiOr4G(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetInfo != null) {
                if (activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    return true;
                }
                if (activeNetInfo.getSubtype() == TelephonyManager.NETWORK_TYPE_LTE) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 获取wap网络条件下的代理服务器，如果为非wap返回为空对象
     *
     * @param context
     * @return
     */
    public static String getHostbyWAP(Context context) {

        if (null == context) {
            return null;
        }

        if (isWifi(context)) {
            return null;
        }

        try {
            String result = null;
            ConnectivityManager mag = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (null != mag) {
                NetworkInfo mobInfo = mag.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                if (null != mobInfo) {

                    String extrainfo = mobInfo.getExtraInfo();
                    if (null != extrainfo) {
                        extrainfo = extrainfo.toLowerCase();
                        if (extrainfo.equals(CMWAP) || extrainfo.equals(WAP_3G) || extrainfo.equals(UNIWAP)) {//移动 or 联通wap代理
                            result = "10.0.0.172";
                        } else {
                            //电信WAP判断
                            Uri PREFERRED_APN_URI = Uri.parse("content://telephony/carriers/preferapn");
                            final Cursor c = context.getContentResolver().query(PREFERRED_APN_URI, null, null, null, null);
                            if (c != null) {
                                c.moveToFirst();
                                final String user = c.getString(c.getColumnIndex("user"));
                                if (!TextUtils.isEmpty(user)) {
                                    if (user.toLowerCase().startsWith(CTWAP)) {
                                        result = "10.0.0.200";
                                    }
                                }
                                c.close();
                            }

                        }
                    }
                }
            }

            return result;
        } catch (Exception e) {
            return null;
        }

    }


    /**
     * 从url获取含有key的参数值
     *
     * @param url
     * @param key
     * @return
     */
    public static String parseURL(String url, String key) {

        String rst = "";

        if (!TextUtils.isEmpty(key)) {
            int i = url.indexOf("?");
            if (i + 1 <= url.length()) {

                String param = url.substring(i + 1, url.length());
                String[] values = param.split("&");
                if (null != values) {

                    for (int index = 0; index < values.length; index++) {

                        if (values[index].contains(key)) {

                            String[] keys = values[index].split("=");

                            if (keys.length > 1) {
                                rst = keys[1];
                                break;
                            }
                        }
                    }
                }

            }
        }

        try {
            return URLDecoder.decode(rst, "GBK");
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }


    /**
     * 在有些机型上获取到的SSID有双引号，需要去掉
     *
     * @param ssid
     * @return
     */
    private static String formatSSID(String ssid) {
        if (TextUtils.isEmpty(ssid))
            return "";

        if (ssid.startsWith("\"") && ssid.endsWith("\""))
            ssid = ssid.substring(1, ssid.length() - 1);

        return ssid;
    }

    /**
     * 检查当前网络是否可用
     *
     * @param context
     * @return
     */
    public static boolean checkNetwork(Context context) {
        boolean flag = false;
        try {
            ConnectivityManager cwjManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cwjManager.getActiveNetworkInfo() != null)
                flag = cwjManager.getActiveNetworkInfo().isConnected();//.isAvailable();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return flag;
    }
}
