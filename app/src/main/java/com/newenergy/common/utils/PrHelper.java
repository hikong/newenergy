package com.newenergy.common.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.newenergy.ui.base.BaseApplication;

/**
 * Created by liuShuai on 17-4-13.
 * 注：对于xposed插件中禁止使用有关sharePreference， 原因： xposed插件仅仅使用java语言，并非安卓程序。
 */

public class PrHelper {
    public static final String FILE_NAME = "share_data";

    public final static String CONTENT = "content";
    public final static String KEY_LAST_VERSION = "key_last_version";

    private static void putStr(String key, String vaule) {
        SharedPreferences sp = BaseApplication.getInstance().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, vaule);
        editor.apply();
    }

    private static String getStr(String key, String defValue) {
        SharedPreferences sp = BaseApplication.getInstance().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        return sp.getString(key, defValue);
    }

    private static void putBoolean(String key, boolean vaule) {
        SharedPreferences sp = BaseApplication.getInstance().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(key, vaule);
        editor.apply();
    }

    private static boolean getBoolean(String key, boolean defValue) {
        SharedPreferences sp = BaseApplication.getInstance().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        return sp.getBoolean(key, defValue);
    }

    private static void removeStr(String key) {
        SharedPreferences sp = BaseApplication.getInstance().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        editor.apply();
    }

    private static void putInt(String key, int value) {
        SharedPreferences sp = BaseApplication.getInstance().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    private static int getInt(String key, int defValue) {
        SharedPreferences sp = BaseApplication.getInstance().getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        return sp.getInt(key, defValue);
    }

    //session
    public final static String PR_SESSION = "pr_session";

    public final static void setPrSession(String token) {
        putStr(PR_SESSION, token);
    }

    public final static String getPrSession() {
        return getStr(PR_SESSION, "");
    }


}
