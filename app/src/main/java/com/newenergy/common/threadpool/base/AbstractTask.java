package com.newenergy.common.threadpool.base;

import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;

/**
 * 抽象的任务类
 * Created by jason on 2017/4/12.
 */
public abstract class AbstractTask<Params, Progress, Result> implements Task<Params> {

    @WorkerThread
    protected abstract Result doInBackground(Params... params);

    @WorkerThread
    protected abstract void publishProgress(Progress... values);

    @MainThread
    protected abstract void onPreExecute();

    @MainThread
    protected abstract void onPostExecute(Result result);

    @MainThread
    protected abstract void onProgressUpdate(Progress... values);

    @MainThread
    protected abstract void onCancelled(Result result);

    @MainThread
    protected abstract void onCancelled();

    @Override
    public void run() {
    }
}
