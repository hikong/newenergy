package com.newenergy.common.threadpool.base;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.MainThread;
import android.support.annotation.WorkerThread;
import android.util.Log;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 任务
 * <p>
 * Params doInBackground执行参数
 * Progress publishProgress进度反馈
 * Result doInBackground执行结果
 * <p>
 * sTaskIdCreater TaskId的创造者
 * sHandler 用于回调到MainThread
 * sThreadPool 是线程池
 * <p>
 * mWorker 是具体执行者
 * mActualTask 是实际执行任务
 * <p>
 * mStatus 标记状态
 * mCancelled mInvoked 取消和激活的状态
 * <p>
 * Created by jason on 2017/4/1.
 */

public abstract class BaseTask<Params, Progress, Result> extends AbstractTask<Params, Progress, Result> {

    private static final String TAG = BaseTask.class.getSimpleName();

    private static final int MESSAGE_POST_RESULT = 0x1;
    private static final int MESSAGE_POST_PROGRESS = 0x2;

    private final long mTaskId;
    private static final AtomicLong sTaskIdCreater = new AtomicLong(1);
    private volatile BaseTask.Status mStatus = BaseTask.Status.PENDING;
    private final AtomicBoolean mCancelled = new AtomicBoolean();
    private final AtomicBoolean mInvoked = new AtomicBoolean();
    private int mPriority = Priority.NORMAL;

    private static InternalHandler sHandler;
    private static volatile ThreadPool sThreadPool;
    private final WorkerRunnable<Params, Result> mWorker;
    private final ActualTask<Result> mActualTask;

    public BaseTask() {
        mWorker = new WorkerRunnable<Params, Result>() {
            public Result call() throws Exception {
                mInvoked.set(true);
                Result result = doInBackground(mParams);
                return postResult(result);
            }
        };

        mActualTask = new ActualTask<Result>(mWorker) {
            @Override
            protected void done(Result result) {
                try {
                    postResultIfNotInvoked(result);
                } catch (Exception e) {
                    Log.e(TAG, e.getLocalizedMessage(), e);
                }
            }
        };

        mTaskId = sTaskIdCreater.getAndIncrement();
    }

    @Override
    @WorkerThread
    protected abstract Result doInBackground(Params... params);

    @Override
    @WorkerThread
    protected void publishProgress(Progress... values) {
        if (!isCancelled()) {
            getHandler().obtainMessage(MESSAGE_POST_PROGRESS,
                    new NetEaseTaskResult<>(this, values)).sendToTarget();
        }
    }

    @Override
    @MainThread
    protected void onPreExecute() {
    }

    @Override
    @MainThread
    protected void onPostExecute(Result result) {
    }

    @Override
    @MainThread
    protected void onProgressUpdate(Progress... values) {
    }

    @Override
    @MainThread
    protected void onCancelled(Result result) {
        onCancelled();
    }

    @Override
    @MainThread
    protected void onCancelled() {
    }

    @Override
    @MainThread
    public AbstractTask<Params, Progress, Result> execute(Params... params) {
        if (mStatus != Status.PENDING) {
            switch (mStatus) {
                case RUNNING:
                    throw new IllegalStateException("Cannot execute task:"
                            + " the task is already running.");
                case FINISHED:
                    throw new IllegalStateException("Cannot execute task:"
                            + " the task has already been executed "
                            + "(a task can be executed only once)");
            }
        }

        mStatus = Status.RUNNING;

        onPreExecute();

        if (mWorker != null) {
            mWorker.mParams = params;
        }

        if (sThreadPool == null) {
            sThreadPool = ThreadPoolManager.getInstance();
        }

        if (mActualTask != null) {
            sThreadPool.execute(mActualTask);
        }

        return this;
    }

    public Task withPriority(int priority) {
        mPriority = priority;
        return this;
    }

    @Override
    public int getPriority() {
        return mPriority;
    }

    @Override
    public long getTaskId() {
        return mTaskId;
    }

    @Override
    public final void cancel() {
        mCancelled.set(true);
        if (mActualTask != null) {
            mActualTask.cancel();
        }
        if (sThreadPool != null) {
            sThreadPool.remove(mActualTask);
        }
    }

    public static void cancel(long taskId) {
        synchronized (AsyncTask.class) {
            if (sThreadPool != null) {
                sThreadPool.remove(taskId);
            }
        }
    }

    private static Handler getHandler() {
        synchronized (AsyncTask.class) {
            if (sHandler == null) {
                sHandler = new InternalHandler();
            }
            return sHandler;
        }
    }

    @Override
    public final Status getStatus() {
        return mStatus;
    }

    public final boolean isCancelled() {
        return mCancelled.get();
    }

    public final boolean isInvoked() {
        return mInvoked.get();
    }

    private void postResultIfNotInvoked(Result result) {
        if (!isInvoked()) {
            postResult(result);
        }
    }

    private Result postResult(Result result) {
        Message message = getHandler().obtainMessage(MESSAGE_POST_RESULT,
                new NetEaseTaskResult<>(this, result));
        message.sendToTarget();
        return result;
    }

    private void finish(Result result) {
        if (isCancelled()) {
            onCancelled(result);
        } else {
            onPostExecute(result);
        }
        mStatus = Status.FINISHED;
    }

    private static abstract class WorkerRunnable<Params, Result> implements Callable<Result> {
        Params[] mParams;
    }

    private class ActualTask<Result> implements Task<Params> {

        private final WorkerRunnable<Params, Result> worker;
        private Result result;

        public ActualTask(WorkerRunnable<Params, Result> worker) {
            this.worker = worker;
        }

        @Override
        public void run() {
            try {
                if (worker != null) {
                    result = worker.call();
                }
                done(result);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        protected void done(Result result) {
        }

        @Override
        public Status getStatus() {
            return mStatus;
        }

        @Override
        public long getTaskId() {
            return mTaskId;
        }

        @Override
        public void cancel() {
            done(null);
        }

        @Override
        public Task<Params> execute(Params... params) {
            return null;
        }

        @Override
        public int getPriority() {
            return mPriority;
        }
    }

    private static class InternalHandler extends Handler {
        public InternalHandler() {
            super(Looper.getMainLooper());
        }

        @Override
        public void handleMessage(Message msg) {
            NetEaseTaskResult<?> result = (NetEaseTaskResult<?>) msg.obj;
            switch (msg.what) {
                case MESSAGE_POST_RESULT:
                    if (result.mData == null || result.mData.length == 0) {
                        result.mTask.finish(null);
                    } else {
                        result.mTask.finish(result.mData[0]);
                    }
                    break;
                case MESSAGE_POST_PROGRESS:
                    result.mTask.onProgressUpdate(result.mData);
                    break;
                default:
                    break;
            }
        }
    }

    private static class NetEaseTaskResult<Data> {
        final BaseTask mTask;
        final Data[] mData;

        NetEaseTaskResult(BaseTask task, Data... data) {
            mTask = task;
            mData = data;
        }
    }
}
