package com.newenergy.common.threadpool;

import android.support.annotation.NonNull;

import com.newenergy.common.threadpool.base.BaseTask;
import com.newenergy.common.threadpool.base.Task;
import com.newenergy.common.threadpool.base.TaskException;


/**
 * 构造子线程处理
 * <p>
 * runInBackground(Runnable) 不处理返回结果
 * runInBackground(NRRunnable) 通过postOnUiThread处理返回结果
 * runNRTask(NRTask) 执行NRTask
 * <p>
 * Created by jason on 2017/4/11.
 */
public class NRThreadPool {

    /**
     * 调用子线程处理 无需处理返回结果
     *
     * @param runnable
     * @return
     */
    public static Task runInBackground(@NonNull final Runnable runnable) {
        Task task = null;
        try {
            task = runInBackground(runnable, Task.Priority.NORMAL);
        } catch (TaskException e) {
            e.printStackTrace();
        } finally {
            return task;
        }
    }

    /**
     * 调用子线程处理 无需处理返回结果
     *
     * @param runnable
     * @param priority
     * @return
     */
    public static Task runInBackground(@NonNull final Runnable runnable, final int priority)
            throws TaskException {
        if (priority < Task.Priority.SHORT || priority > Task.Priority.VENTI) {
            throw new TaskException("Runnable Priority Value from SHORT to VENTI");
        }
        Task task = new BaseTask<Object, Long, Object>() {
            @Override
            protected Object doInBackground(Object... params) {
                runnable.run();
                return null;
            }
        }.withPriority(priority).execute();
        return task;
    }

    /**
     * 调用子线程处理 通过postOnUiThread处理返回结果
     *
     * @param runnable
     * @return
     */
    public static Task runInBackground(@NonNull final NRRunnable runnable) {
        Task task = null;
        try {
            task = runInBackground(runnable, Task.Priority.NORMAL);
        } catch (TaskException e) {
            e.printStackTrace();
        } finally {
            return task;
        }
    }

    /**
     * 调用子线程处理 通过postOnUiThread处理返回结果
     *
     * @param runnable
     * @param priority
     * @return
     */
    public static Task runInBackground(@NonNull final NRRunnable runnable, final int priority)
            throws TaskException {
        if (priority < Task.Priority.SHORT || priority > Task.Priority.VENTI) {
            throw new TaskException("Runnable Priority Value from SHORT to VENTI");
        }
        Task task = new BaseTask<Object, Long, Object>() {
            @Override
            protected Object doInBackground(Object... params) {
                return runnable.run();
            }

            @Override
            protected void onPostExecute(Object result) {
                runnable.postOnUiThread(result);
            }
        }.withPriority(priority).execute();
        return task;
    }

    /**
     * 执行NRTask
     *
     * @param nrTask
     * @return
     */
    public static Task runNRTask(@NonNull final NRTask nrTask) {
        Task task = null;
        try {
            task = runNRTask(nrTask, nrTask.getPriority());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return task;
        }
    }

    /**
     * 执行NRTask
     *
     * @param nrTask
     * @param priority
     * @return
     */
    public static Task runNRTask(@NonNull final NRTask nrTask, final int priority) {
        if (priority < Task.Priority.SHORT || priority > Task.Priority.VENTI) {
            throw new IllegalArgumentException("Runnable Priority Value from SHORT to VENTI");
        }
        nrTask.withPriority(priority).execute();
        return nrTask;
    }
}
