package com.newenergy.common.threadpool;


import com.newenergy.common.threadpool.base.BaseTask;

/**
 * 构造任务在子线程中处理
 * <p>
 * doInBackground() 重构改造成无参数方法
 * onProgressUpdate() 参数默认为Long
 * <p>
 * Created by jason on 2017/4/11.
 */

public abstract class NRTask<Result> extends BaseTask<Object, Long, Result> {

    protected abstract Result doInBackground();

    @Override
    protected Result doInBackground(Object... params) {
        return doInBackground();
    }
}
