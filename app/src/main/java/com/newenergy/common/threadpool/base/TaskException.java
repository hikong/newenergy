package com.newenergy.common.threadpool.base;

/**
 * Created by jason on 2017/4/13.
 */

public class TaskException extends Exception {

    public TaskException(String msg) {
        super(msg);
    }
}
