package com.newenergy.common.threadpool;

/**
 * 封装处理返回结果的子线程
 * <p>
 * run() 在子线程中执行
 * postOnUiThread() 处理返回结果
 * <p>
 * Created by jason on 2017/4/11.
 */

public interface NRRunnable<Result> {

    Result run();

    void postOnUiThread(Result result);
}
