package com.newenergy.common.threadpool.base;

/**
 * 对外提供
 * <p>
 * getStatus() 获取状态
 * getTaskId() 获取TaskId
 * cancel() 取消任务接口
 * <p>
 * Created by jason on 2017/4/1.
 */

public interface Task<Params> extends Runnable {

    Status getStatus();

    long getTaskId();

    void cancel();

    Task<Params> execute(Params... params);

    int getPriority();

    /**
     * 任务状态
     */
    enum Status {
        PENDING,
        RUNNING,
        FINISHED,
    }

    /**
     * 任务优先级
     * 使用星巴克计量单位 从小到大 short tall normal grande venti
     * normal是默认值
     */
    class Priority {
        public static int SHORT = 1;
        public static int TALL = SHORT + 1;
        public static int NORMAL = TALL + 1;
        public static int GRANDE = NORMAL + 1;
        public static int VENTI = GRANDE + 1;
    }
}
