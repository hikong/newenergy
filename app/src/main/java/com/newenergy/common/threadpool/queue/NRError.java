package com.newenergy.common.threadpool.queue;

import android.os.Bundle;

public interface NRError {

    void doing(Throwable error, Bundle args);
}