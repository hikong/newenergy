package com.newenergy.ui.analysis;

import android.content.Context;
import android.graphics.Color;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.newenergy.R;
import com.newenergy.ui.analysis.bean.PieBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ls on 2018/3/4.
 */

public class ChartUtils {

    public static BarChart createBarChart(Context context, List<Integer> yVals, final List<String> xVals, String title) {

        List<BarEntry> mDatas = new ArrayList<>();

        for (int i = 0; i < yVals.size(); i++) {
            mDatas.add(new BarEntry(i, yVals.get(i)));

        }
        BarChart barChart = new BarChart(context);
        BarDataSet dataSet = new BarDataSet(mDatas, title);
        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        barChart.setData(new BarData(dataSet));
        barChart.setDescription(null);
        barChart.animateY(1000);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xVals.get((int) value);
            }
        });
        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setEnabled(false);


        barChart.setTouchEnabled(false);
        return barChart;
    }

    public static LineChart createLineChart(Context context, List<Integer> yVals, final List<String> xVals, String title) {

        List<Entry> mDatas = new ArrayList<>();

        for (int i = 0; i < yVals.size(); i++) {
            mDatas.add(new Entry(i, yVals.get(i)));

        }
        LineChart barChart = new LineChart(context);
        LineDataSet dataSet = new LineDataSet(mDatas, title);
        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        barChart.setData(new LineData(dataSet));
        barChart.setDescription(null);
        barChart.animateY(1000);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1);
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xVals.get((int) value);
            }
        });
        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setEnabled(false);


        barChart.setTouchEnabled(false);
        return barChart;
    }

    public static PieChart createPieChart(Context context, List<PieBean.SeriesBean.DataBean> val, String title) {

        List<PieEntry> mDatas = new ArrayList<>();

        for (int i = 0; i < val.size(); i++) {
            mDatas.add(new PieEntry(val.get(i).getValue(), val.get(i).getName()));

        }
        PieChart chart = new PieChart(context);
        PieDataSet dataSet = new PieDataSet(mDatas, title);
        //数据和颜色
        ArrayList<Integer> colors = new ArrayList<Integer>();
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
        colors.add(ColorTemplate.getHoloBlue());
        dataSet.setColors(colors);

        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        chart.setData(new PieData(dataSet));
        chart.setDescription(null);
        chart.animateY(1000);

        chart.setTouchEnabled(false);

        Legend l = chart.getLegend();
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setFormSize(18f);//比例块字体大小


        // 设置饼图是否接收点击事件，默认为true
        chart.setTouchEnabled(true);
        //设置饼图是否使用百分比
        chart.setUsePercentValues(true);

        //设置圆盘是否转动，默认转动
        chart.setRotationEnabled(true);
        //设置初始旋转角度
        chart.setRotationAngle(100);
        chart.setEntryLabelTextSize(40);

        chart.setDrawSliceText(false);//设置隐藏饼图上文字，只显示百分比

        return chart;
    }
}
