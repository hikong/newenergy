package com.newenergy.ui.analysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;

import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.newenergy.R;
import com.newenergy.ui.base.event.MessageEvent;
import com.newenergy.ui.base.fragment.ABaseFragment;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

public class UsageRefrigeFragment extends ABaseFragment {


    @BindView(R.id.tabLayout)
    SegmentTabLayout mTabLayout;

    private String[] mTitles = new String[]{"柱状图", "折线图", "饼状图"};

    @Override
    public int inflateContentView() {
        return R.layout.fragment_data_analysis_refrige;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTabLayout.setTabData(mTitles);
        initChartFragment();
        mTabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                changeTab(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

    }

    private void changeTab(int position) {
        String type = "";
        switch (position) {
            case 0:
                type = UsageRefrigeChartViewFragment.TYPE_BAR;
                break;
            case 1:
                type = UsageRefrigeChartViewFragment.TYPE_LINE;
                break;
            case 2:
                type = UsageRefrigeChartViewFragment.TYPE_PIE;
                break;
            default:
                break;
        }
        if (!TextUtils.isEmpty(type)) {
            EventBus.getDefault().post(new MessageEvent(type));
        }

    }

    private void initChartFragment() {
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        Fragment fragment = fm.findFragmentByTag(UsageRefrigeChartViewFragment.class.getName());
        if (fragment == null) {
            fragment = Fragment.instantiate(getContext(), UsageRefrigeChartViewFragment.class.getName(), null);
            transaction.add(R.id.container, fragment, UsageRefrigeChartViewFragment.class.getName());

        } else {
            if (fragment.isDetached()) {
                transaction.attach(fragment);
            } else {
                transaction.add(R.id.container, fragment, UsageRefrigeChartViewFragment.class.getName());
            }
        }
        transaction.commit();
    }
}
