package com.newenergy.ui.analysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarEntry;
import com.google.gson.reflect.TypeToken;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.analysis.adapter.RadioAdapter;
import com.newenergy.ui.analysis.bean.BarBean;
import com.newenergy.ui.analysis.bean.PieBean;
import com.newenergy.ui.base.event.MessageEvent;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.IResponseListener;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.base.network.task.VolleyManager;
import com.newenergy.ui.base.widge.DatePickView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class UsageRefrigeChartViewFragment extends ABaseRequestFragment<BlockBean> {
    public final static String TAG = UsageRefrigeChartViewFragment.class.getName();
    public final static String TYPE_BAR = "bar";
    public final static String TYPE_LINE = "line";
    public final static String TYPE_PIE = "pie";

    @BindView(R.id.datePickView)
    DatePickView mDatePickView;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private RadioAdapter mRadioAdapter;

    private List<BarEntry> mDatas = new ArrayList<>();
    private String mType;


    @BindView(R.id.chartContainer)
    FrameLayout mFrameLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_data_analysis_comm;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDatePickView.setOnDateChangeListener(new DatePickView.DataChangeListener() {
            @Override
            public void onChange() {

            }
        });

        //recyclerView
        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));

        //默认是柱状图
        mType = TYPE_BAR;
    }

    @Override
    protected BaseVolleyRequest<BlockBean> createNetRequest(boolean isRefresh) {
        Request r = RequestDefine.getColdBlock();
        return new CommonRequest<>(r, new IParseNetwork<BlockBean>() {
            @Override
            public BlockBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, new TypeToken<BlockBean>() {
                });
            }
        });
    }

    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, BlockBean response) {
        super.onResponse(isNetResponse, isRefresh, response);
        System.out.println(response.getBlock().size());
        if (response != null && response.getBlock() != null && !response.getBlock().isEmpty()) {
            final List<List<String>> blocks = response.getBlock();
            //radio
            if (mRadioAdapter == null) {
                mRadioAdapter = new RadioAdapter(getContext(), blocks);
            }
            mRecyclerView.setAdapter(mRadioAdapter);
        }


    }


    private void updateChart() {
        //chart
//        BarDataSet dataSet = new BarDataSet(mDatas, "用冷量");
//        //dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
//        mBarChart.setData(new BarData(dataSet));
//        mBarChart.setDescription(null);
//        mBarChart.animateY(2000);
//        XAxis xAxis = mBarChart.getXAxis();
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.setDrawAxisLine(true);
//        xAxis.setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float v, AxisBase axisBase) {
//                //return blocks.get((int) v).get(0);
//                return null;
//            }
//        });

    }

    @OnClick({R.id.check})
    void OnClick(View v) {
        switch (v.getId()) {
            case R.id.check:
                check();
                break;
        }
    }

    private void check() {
        if (mRadioAdapter != null) {
            List<List<String>> blocks = mRadioAdapter.getSelectins();
            String blockIds = "";
            String blockNames = "";

            for (int i = 0; i < blocks.size(); i++) {
                blockNames += blocks.get(i).get(0) + ",";
                blockIds += blocks.get(i).get(1) + ",";
                if (i == blocks.size() - 1) {
                    blockIds = blockIds.substring(0, blockIds.length() - 1);
                    blockNames = blockNames.substring(0, blockNames.length() - 1);
                }
            }
            if (TextUtils.isEmpty(blockIds) || TextUtils.isEmpty(blockNames)) {
                Toast.makeText(getContext(), "请选择地块信息", Toast.LENGTH_SHORT).show();
            }

            String type = "";
            if (mType.equals(TYPE_BAR)) {
                type = "getcolkdata";
            } else if (mType.equals(TYPE_LINE)) {
                type = "getcoldlinedata";
            } else if (mType.equals(TYPE_PIE)) {
                type = "getcoldpiedata";
            }
            Request r = RequestDefine.getBlcokColdInfo(type, mDatePickView.getLeftTime(), mDatePickView.getRightTime(), blockIds, blockNames);
            final CommonRequest<String> request = new CommonRequest<String>(r, new IParseNetwork<String>() {
                @Override
                public String parseNetworkResponse(String jsonStr) {
                    return jsonStr;
                }
            });

            request.setResponseListener(new IResponseListener<String>() {
                @Override
                public void onResponse(int requestId, String response) {
                    setBarData(response);
                }

                @Override
                public void onErrorResponse(int requestId, VolleyError error) {
                    Toast.makeText(getContext(), error != null && TextUtils.isEmpty(error.getMessage()) ? error.getMessage() : "数据加载失败", Toast.LENGTH_SHORT).show();

                }
            });
            VolleyManager.addRequest(request);

        }
    }

    private void setBarData(String json) {
        if (mType.equals(TYPE_BAR)) {
            BarBean barBean = JsonUtils.fromJson(json, BarBean.class);
            if (barBean != null && barBean.getSeries() != null && !barBean.getSeries().isEmpty()) {
                BarChart chart = ChartUtils.createBarChart(getContext(), barBean.getSeries().get(0).getData(), barBean.getCategory(), "用冷分析");
                if (mFrameLayout != null) {
                    if (mFrameLayout.getChildCount() > 0) {
                        mFrameLayout.removeAllViews();
                    }
                    mFrameLayout.addView(chart);
                }
            } else {
                Toast.makeText(getContext(), barBean != null && !TextUtils.isEmpty(barBean.getMsg()) ? barBean.getMsg() : "数据为空", Toast.LENGTH_SHORT).show();
            }
        } else if (mType.equals(TYPE_LINE)) {
            json = "{\"series\":[{\"name\":\"用冷量\",\"type\":\"line\",\"data\":[0,10,20,30,40,0,0]}],\"category\":[\"01-22\",\"01-23\",\"01-21\",\"01-17\",\"01-19\",\"01-20\",\"01-18\"],\"status\":1,\"legend\":[\"用冷量\"]}";
            BarBean barBean = JsonUtils.fromJson(json, BarBean.class);
            if (barBean != null && barBean.getSeries() != null && !barBean.getSeries().isEmpty()) {
                LineChart chart = ChartUtils.createLineChart(getContext(), barBean.getSeries().get(0).getData(), barBean.getCategory(), "用冷分析");
                if (mFrameLayout != null) {
                    if (mFrameLayout.getChildCount() > 0) {
                        mFrameLayout.removeAllViews();
                    }
                    mFrameLayout.addView(chart);
                }
            } else {
                Toast.makeText(getContext(), barBean != null && !TextUtils.isEmpty(barBean.getMsg()) ? barBean.getMsg() : "数据为空", Toast.LENGTH_SHORT).show();
            }
        } else if (mType.equals(TYPE_PIE)) {
            PieBean pieBean = JsonUtils.fromJson(json, PieBean.class);

            if (pieBean != null && pieBean.getSeries() != null && !pieBean.getSeries().isEmpty()) {
                PieChart chart = ChartUtils.createPieChart(getContext(), pieBean.getSeries().get(0).getData(), "用冷分析");
                if (mFrameLayout != null) {
                    if (mFrameLayout.getChildCount() > 0) {
                        mFrameLayout.removeAllViews();
                    }
                    mFrameLayout.addView(chart);
                }
            }else {
                Toast.makeText(getContext(), pieBean != null && !TextUtils.isEmpty(pieBean.getMsg()) ? pieBean.getMsg() : "数据为空", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public BlockBean loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(BlockBean response) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {/* Do something */
        mType = event.getType();
        Log.d(TAG, mType);
    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }


}
