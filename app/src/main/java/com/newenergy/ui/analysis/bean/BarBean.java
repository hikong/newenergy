package com.newenergy.ui.analysis.bean;

import com.google.gson.annotations.SerializedName;
import com.newenergy.ui.base.bean.BaseBean;

import java.util.List;

/**
 * Created by ls on 2018/3/4.
 */

public class BarBean extends BaseBean{
    /**
     * series : [{"name":"用电","type":"bar","data":[0,0,0,0,0]}]
     * category : ["01-19","01-20","01-21","01-22","01-23"]
     * status : 1
     * legend : ["用电"]
     */

    private List<SeriesBean> series;
    private List<String> category;
    private List<String> legend;

    public List<SeriesBean> getSeries() {
        return series;
    }

    public void setSeries(List<SeriesBean> series) {
        this.series = series;
    }

    public List<String> getCategory() {
        return category;
    }

    public void setCategory(List<String> category) {
        this.category = category;
    }

    public List<String> getLegend() {
        return legend;
    }

    public void setLegend(List<String> legend) {
        this.legend = legend;
    }

    public static class SeriesBean {
        /**
         * name : 用电
         * type : bar
         * data : [0,0,0,0,0]
         */

        private String name;
        private String type;
        private List<Integer> data;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<Integer> getData() {
            return data;
        }

        public void setData(List<Integer> data) {
            this.data = data;
        }
    }
}
