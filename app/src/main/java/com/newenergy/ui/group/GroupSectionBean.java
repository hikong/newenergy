package com.newenergy.ui.group;

import android.text.TextUtils;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.chad.library.adapter.base.entity.SectionEntity;

import java.io.Serializable;

/**
 * Created by ls on 17/9/18.
 */

public class GroupSectionBean extends AbstractExpandableItem<GroupUserBean.GroupUsersBean.UsersBean>  implements MultiItemEntity, Serializable {

    public String groupTitle;
    public int groupId;

    public GroupSectionBean(String groupTitle, int groupId) {
        this.groupTitle = groupTitle;
        this.groupId = groupId;
    }

    @Override
    public int getLevel() {
        return 0;
    }

    @Override
    public int getItemType() {
        return GroupSectionAdapter.TYPE_LEVEL_0 ;
    }
}
