package com.newenergy.ui.group;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.VolleyError;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.bean.BaseResponseBean;
import com.newenergy.ui.base.fragment.ABaseFragment;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.IResponseListener;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.Response;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.base.network.task.VolleyManager;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ls on 17/9/18.
 */

public class GroupMoveFragment extends ABaseFragment {

    @BindView(R.id.icon)
    TextView mIcon;
    private List<GroupSectionBean> mDatas;
    private GroupUserBean.GroupUsersBean.UsersBean mUsersBean;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDatas = (List<GroupSectionBean>) getArguments().getSerializable("group");
            mUsersBean = (GroupUserBean.GroupUsersBean.UsersBean) getArguments().getSerializable("child");
        }
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_group_move;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mUsersBean != null) {
            mIcon.setText(mUsersBean.getUser_name());
        }
    }

    @OnClick(R.id.btn_move)
    void OnClick(View v) {
        switch (v.getId()) {
            case R.id.btn_move:
                showDialog();
                break;
        }
    }

    private void showDialog() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getContext());
        //创建recyclerView
        RecyclerView recyclerView = new RecyclerView(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        final RecyclerAdapter recyclerAdapter = new RecyclerAdapter(mDatas, mUsersBean);
        recyclerView.setAdapter(recyclerAdapter);
        recyclerAdapter.setOnItemClickListener(new RecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (adapter != null && adapter.getItem(position) != null) {
                    Intent i = new Intent();
                    i.putExtra("child", mUsersBean);
                    getActivity().setResult(Activity.RESULT_OK, i);
                    final GroupSectionBean item = ((GroupSectionBean) adapter.getItem(position));
                    if (item != null) {
                        final String groupId = String.valueOf(item.groupId);
                        String userId = String.valueOf(mUsersBean.getUser_id());
                        Request r = RequestDefine.modifyUserGroup(userId, groupId);
                        CommonRequest<BaseResponseBean> request = new CommonRequest<BaseResponseBean>(r, new IParseNetwork<BaseResponseBean>() {
                            @Override
                            public BaseResponseBean parseNetworkResponse(String jsonStr) {
                                return JsonUtils.fromJson(jsonStr, BaseResponseBean.class);
                            }
                        });
                        final MaterialDialog dialog = new MaterialDialog.Builder(getActivity())
                                .content("分组移动中")
                                .progress(true, 0)
                                .build();
                        dialog.show();

                        request.setResponseListener(new IResponseListener<BaseResponseBean>() {
                            @Override
                            public void onResponse(int requestId, BaseResponseBean response) {
                                if (response != null && Response.MSG_OK.equals(response.getStatus())) {
                                    Toast.makeText(getActivity(), response.getMsg(), Toast.LENGTH_SHORT).show();
                                    mUsersBean.setGroupId(item.groupId);
                                    recyclerAdapter.notifyDataSetChanged();
                                }
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            }

                            @Override
                            public void onErrorResponse(int requestId, VolleyError error) {
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            }
                        });
                        VolleyManager.addRequest(request);
                    }
                }
            }
        });
        bottomSheetDialog.setContentView(recyclerView);
        bottomSheetDialog.show();
    }

    private static class RecyclerAdapter extends BaseQuickAdapter<GroupSectionBean, BaseViewHolder> {
        private GroupUserBean.GroupUsersBean.UsersBean mUsersBean;

        public RecyclerAdapter(@Nullable List<GroupSectionBean> data, GroupUserBean.GroupUsersBean.UsersBean usersBean) {
            super(R.layout.item_group_move, data);
            mUsersBean = usersBean;
        }

        @Override
        protected void convert(BaseViewHolder helper, final GroupSectionBean item) {
            helper.setText(R.id.group, item.groupTitle);
            helper.setImageResource(R.id.group_icon,
                    mContext.getResources().getString(R.string.group_manager).equals(item.groupTitle) ? R.drawable.ic_group_a : R.drawable.ic_group_b);

            if (mUsersBean != null && mUsersBean.getGroupId() == item.groupId) {
                helper.setVisible(R.id.group_select, true);
            } else {
                helper.setVisible(R.id.group_select, false);
            }
        }
    }
}
