package com.newenergy.ui.patrol.upload;

import com.newenergy.ui.base.bean.BaseBean;

import java.util.List;

/**
 * Created by liushuai on 2017/9/25.
 */

public class FileUploadBean extends BaseBean {

    private List<FilepathBean> filepath;

    public List<FilepathBean> getFilepath() {
        return filepath;
    }

    public void setFilepath(List<FilepathBean> filepath) {
        this.filepath = filepath;
    }

    public static class FilepathBean extends BaseBean{
        /**
         * picurl : 16e851a0af484367a9bc2582f993ecef.png
         * picname : 16e851a0af484367a9bc2582f993ecef.png
         */

        private String picurl;
        private String picname;

        public String getPicurl() {
            return picurl;
        }

        public void setPicurl(String picurl) {
            this.picurl = picurl;
        }

        public String getPicname() {
            return picname;
        }

        public void setPicname(String picname) {
            this.picname = picname;
        }
    }
}
