package com.newenergy.ui.patrol.upload;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.lzy.imagepicker.ui.ImagePreviewDelActivity;
import com.lzy.imagepicker.view.CropImageView;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.ABaseFragment;
import com.newenergy.ui.base.network.request.IResponseListener;
import com.newenergy.ui.base.network.request.MutipartRequestUpload;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

import java.io.File;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by ls on 17/9/17.
 */

public class PicSelectFragment extends ABaseFragment implements PicPickerAdapter.OnRecyclerViewItemClickListener {
    public final static String PARAM_QUERYID = "PARAM_QUERYID";
    public final static String PARAM_COUNT = "PARAM_COUNT";
    private static final int REQUEST_CODE_SELECT = 100;
    public static final int REQUEST_CODE_PREVIEW = 101;

    private static final int EXTERNAL_STORAGE_PERMISSION = 1;
    public static final int PARAM_MAX_COUNT = 9;
    private int mMaxCount = PARAM_MAX_COUNT;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;//图片列表
    private ArrayList<ImageItem> mSelImageList = new ArrayList<>(); //当前选择的所有图片

    private PicPickerAdapter mAdapter;
    protected String mId;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle(getResources().getString(R.string.title_patrol));

        if (getArguments() != null) {
            mId = getArguments().getString(PARAM_QUERYID);
            mMaxCount = getArguments().getInt(PARAM_COUNT, PARAM_MAX_COUNT);
        }else {
            throw new InvalidParameterException();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initImagePicker();
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_pic_select;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new PicPickerAdapter(mSelImageList);
        mAdapter.setOnItemClickListener(this);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }


    private void initImagePicker() {
        ImagePicker imagePicker = ImagePicker.getInstance();
        imagePicker.setImageLoader(new GlideImageLoader());   //设置图片加载器
        imagePicker.setShowCamera(true);                      //显示拍照按钮
        imagePicker.setCrop(false);                           //允许裁剪（单选才有效）
        imagePicker.setSaveRectangle(true);                   //是否按矩形区域保存
        imagePicker.setSelectLimit(mMaxCount);              //选中数量限制
        imagePicker.setStyle(CropImageView.Style.RECTANGLE);  //裁剪框的形状
        //imagePicker.setFocusWidth(800);                       //裁剪框的宽度。单位像素（圆形自动取宽高最小值）
        //imagePicker.setFocusHeight(800);                      //裁剪框的高度。单位像素（圆形自动取宽高最小值）
        //imagePicker.setOutPutX(1000);                         //保存文件的宽度。单位像素
        //imagePicker.setOutPutY(1000);                         //保存文件的高度。单位像素
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_STORAGE_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //goToScanActivity();
                    goToPicSelect();
                } else {
                    Toast.makeText(getActivity(), "请开权限", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ArrayList<ImageItem> images;
        if (resultCode == ImagePicker.RESULT_CODE_ITEMS) {
            //添加图片返回
            if (data != null && requestCode == REQUEST_CODE_SELECT) {
                images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);
                if (images != null) {
                    mSelImageList.clear();
                    mSelImageList.addAll(images);
                    mAdapter.setData(mSelImageList);
                }
            }
        } else if (resultCode == ImagePicker.RESULT_CODE_BACK) {
            //预览图片返回
            if (data != null && requestCode == REQUEST_CODE_PREVIEW) {
                images = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_IMAGE_ITEMS);
                if (images != null) {
                    mSelImageList.clear();
                    mSelImageList.addAll(images);
                    mAdapter.setData(mSelImageList);
                }
            }
        }
    }


    @Override
    public void onItemClick(View view, int position, boolean isAdd) {
        if (isAdd) {
            goToPicSelect();
        } else {
            goToPicPreview(position);
        }
    }

    private void goToPicPreview(int position) {
        //打开预览
        Intent intentPreview = new Intent(getActivity(), ImagePreviewDelActivity.class);
        intentPreview.putExtra(ImagePicker.EXTRA_IMAGE_ITEMS, mSelImageList);
        intentPreview.putExtra(ImagePicker.EXTRA_SELECTED_IMAGE_POSITION, position);
        intentPreview.putExtra(ImagePicker.EXTRA_FROM_ITEMS, true);
        startActivityForResult(intentPreview, REQUEST_CODE_PREVIEW);
    }

    private void goToPicSelect() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION);
        } else {

            //打开选择,本次允许选择的数量
//            ImagePicker.getInstance().setSelectLimit(PARAM_MAX_COUNT - mSelImageList.size());
            Intent intent = new Intent(getActivity(), ImageGridActivity.class);
                                /* 如果需要进入选择的时候显示已经选中的图片，
                                 * 详情请查看ImagePickerActivity
                                 * */
            intent.putExtra(ImageGridActivity.EXTRAS_IMAGES, mSelImageList);
            startActivityForResult(intent, REQUEST_CODE_SELECT);
        }
    }

    @OnClick(R.id.commit)
    void OnClick(View v) {
        switch (v.getId()) {
            case R.id.commit:
                if (!readyCommit()) {
                    return;
                }
                //FragmentContainerActivity.launch(getActivity(), RemarkFragment.class, null);
                String url = getUploadUrl();
//                String url = String.format(RequestUrl.POST_UPLOADFILE, mDeviceId);
                List<File> data = new ArrayList<>();
                for (ImageItem imageItem : mSelImageList) {
                    data.add(new File(imageItem.path));
                }
                MutipartRequestUpload<FileUploadBean> requestUpload = new MutipartRequestUpload<>(url, data, new IParseNetwork<FileUploadBean>() {
                    @Override
                    public FileUploadBean parseNetworkResponse(String jsonStr) {
                        return JsonUtils.fromJson(jsonStr, FileUploadBean.class);
                    }
                });
                requestUpload.setResponseListener(new IResponseListener<FileUploadBean>() {
                    @Override
                    public void onResponse(int requestId, FileUploadBean response) {
                        //Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
                        dealResponse(requestId, response);
                    }

                    @Override
                    public void onErrorResponse(int requestId, VolleyError error) {

                    }
                });
                sendRequest(requestUpload);

                break;
        }
    }

    protected boolean readyCommit() {
        return true;
    }

    protected String getUploadUrl() {
        return "";
    }

    protected void dealResponse(int requestId, FileUploadBean response) {

    }

}
