package com.newenergy.ui.patrol.deviceinfo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by liushuai on 2017/9/4.
 */

public class DeviceListFragment extends ABaseRequestFragment<DeviceInfoBean> {

    @BindView(R.id.recylcerView)

    RecyclerView mRecyclerView;

    private List<DeviceSection> mData = new ArrayList<>();

    private DeviceSectionAdapter mAdapter;


    @Override
    public int inflateContentView() {
        return R.layout.fragment_device_list;
    }

    @Override
    public void setContentView(ViewGroup view) {
        super.setContentView(view);
    }

    @Override
    protected BaseVolleyRequest<DeviceInfoBean> createNetRequest(boolean isRefresh) {
        Request request = RequestDefine.getDeviceInfo();
        return new CommonRequest<>(request, new IParseNetwork<DeviceInfoBean>() {
            @Override
            public DeviceInfoBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, DeviceInfoBean.class);
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new DeviceSectionAdapter( mData);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, DeviceInfoBean response) {
        super.onResponse(isNetResponse, isRefresh, response);

        if (response != null && response.getBlock_devs() != null) {
            List<DeviceInfoBean.BlockDevsBean> blockDevsBeanList = response.getBlock_devs(); //总数据
            if (blockDevsBeanList != null && !blockDevsBeanList.isEmpty()) {
                for (DeviceInfoBean.BlockDevsBean blockDevsBean : blockDevsBeanList) {
                    List<DeviceInfoBean.BlockDevsBean.DevicesBean> devicesBeans = blockDevsBean.getDevices();
                    mData.add(new DeviceSection(blockDevsBean.getBlock_name(), String.valueOf(devicesBeans.size())));
                    mData.add(new DeviceSection(devicesBeans));
                }
            }

            mAdapter.setNewData(mData);
        }
    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }


    @Override
    public DeviceInfoBean loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(DeviceInfoBean response) {

    }

}
