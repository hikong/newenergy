package com.newenergy.ui.patrol.viewer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by liushuai on 2017/9/25.
 */

public class PicViewerFragment extends ABaseRequestFragment<PicBean> implements BaseQuickAdapter.OnItemClickListener {
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private String mId;
    private String mType;
    public final static String PARAM_ID = "param_id";
    public final static String PARAM_TYPE = "param_type";

    public final static String PARAM_TYPE_PATROL = "param_type_patrol";
    public final static String PARAM_TYPE_MAINTAIN = "param_type_maintain";
    private PicViewerAdapter mAdapter;
    private List<PicBean.QuemaiimgBean> mQuemaiimg = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mId = getArguments().getString(PARAM_ID);
            mType = getArguments().getString(PARAM_TYPE);
        }
        setActionBarTitle("图片浏览");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new PicViewerAdapter(mQuemaiimg);
        mAdapter.setOnItemClickListener(this);
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public PicBean loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(PicBean response) {

    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }

    @Override
    protected BaseVolleyRequest<PicBean> createNetRequest(boolean isRefresh) {
        Request r = null;
        if (PARAM_TYPE_PATROL.equals(mType)) {
            r = RequestDefine.getQueryMaintainImg(RequestUrl.GET_QUERY_MAINTAIN_IMG, mId);
        } else if (PARAM_TYPE_MAINTAIN.equals(mType)) {
            r = RequestDefine.getQueryMaintainImg(RequestUrl.GET_DEVMAIN_RESULT_IMG, mId);
        }
        if (r != null) {
            return new CommonRequest<>(r, new IParseNetwork<PicBean>() {
                @Override
                public PicBean parseNetworkResponse(String jsonStr) {
                    return JsonUtils.fromJson(jsonStr, PicBean.class);
                }
            });
        }
        return null;
    }

    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, PicBean response) {
        super.onResponse(isNetResponse, isRefresh, response);
        if (isRefresh && response != null && response.getQuemaiimg() != null) {
            mQuemaiimg.clear();
            mQuemaiimg.addAll(response.getQuemaiimg());
            mAdapter.setNewData(mQuemaiimg);
        }

    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_pic_viewer;
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {

    }
}
