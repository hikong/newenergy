package com.newenergy.ui.patrol.deviceinfo;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.equitpment.detail.EquipmentDetailListFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class DeviceSectionAdapter extends BaseSectionQuickAdapter<DeviceSection, BaseViewHolder> {
    public final static String PARAM_DEVICE = "param_device";
    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data             A new list is created out of this one to avoid mutable list
     */
    public DeviceSectionAdapter(List data) {
        super(R.layout.item_patrol_section_child, R.layout.item_patrol_section_head, data);
    }

    @Override
    protected void convertHead(BaseViewHolder helper, final DeviceSection item) {
        helper.setText(R.id.title, item.header);
        helper.setText(R.id.count, item.getCount());
    }


    @Override
    protected void convert(BaseViewHolder helper, DeviceSection item) {
        //DeviceInfoBean.BlockDevsBean.DevicesBean itemBean = item.t;
        //helper.setText(R.id.title, "child");

        List<DeviceInfoBean.BlockDevsBean.DevicesBean> titles = item.t;
        RecyclerView recyclerView = helper.getView(R.id.recyclerView);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        recyclerView.setAdapter(new TitleAdapter(titles));
    }


    class TitleAdapter extends RecyclerView.Adapter<TitleAdapter.MyViewHolder> {

        private  List<DeviceInfoBean.BlockDevsBean.DevicesBean> mDatas = new ArrayList<>();

        public TitleAdapter(List<DeviceInfoBean.BlockDevsBean.DevicesBean> datas) {
            mDatas = datas;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            MyViewHolder holder = new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mathine_name, parent,
                    false));
            return holder;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.tv.setText(mDatas.get(position).getName());
        }

        @Override
        public int getItemCount() {
            return mDatas == null || mDatas.isEmpty() ? 0 : mDatas.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView tv;

            public MyViewHolder(View view) {
                super(view);
                tv = (TextView) view.findViewById(R.id.title);
                tv.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                FragmentArgs args = new FragmentArgs();
                args.add(EquipmentDetailListFragment.PARAM_DEVICE_ID, mDatas.get(getLayoutPosition()).getId());
                args.add(EquipmentDetailListFragment.PARAM_TITLE, mDatas.get(getLayoutPosition()).getName());
                FragmentContainerActivity.launch((BaseActivity)mContext, EquipmentDetailListFragment.class, args);
            }
        }
    }

}
