package com.newenergy.ui.patrol.bean;

import com.google.gson.annotations.SerializedName;
import com.newenergy.ui.base.bean.BaseListBean;

import java.util.List;

/**
 * Created by ls on 17/9/12.
 */

public class PatrolBean extends BaseListBean {

    @SerializedName(value = "querymaintain", alternate = {"devmainlist", "devrepairlist"})
    private List<QuerymaintainBean> querymaintain;

    public List<QuerymaintainBean> getQuerymaintain() {
        return querymaintain;
    }

    public void setQuerymaintain(List<QuerymaintainBean> querymaintain) {
        this.querymaintain = querymaintain;
    }

    public static class QuerymaintainBean {
        /**
         * createtime : 2017-08-26 15:40:39
         * id : 1
         * username : 张三
         * memo : fkl;dfk;dlfk;dlkfsdlsdfdfsd
         * devname : 设备1_1
         * blockname : 地块1
         */

        private String createtime;
        private int id;
        private String username;
        private String memo;
        @SerializedName(value = "devname", alternate = {"name"})
        private String devname;
        private String blockname;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getDevname() {
            return devname;
        }

        public void setDevname(String devname) {
            this.devname = devname;
        }

        public String getBlockname() {
            return blockname;
        }

        public void setBlockname(String blockname) {
            this.blockname = blockname;
        }
    }
}
