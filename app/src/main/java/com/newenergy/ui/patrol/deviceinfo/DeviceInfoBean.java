package com.newenergy.ui.patrol.deviceinfo;

import com.newenergy.ui.base.bean.BaseBean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ls on 17/9/14.
 */

public class DeviceInfoBean extends BaseBean {

    private List<BlockDevsBean> block_devs;

    public List<BlockDevsBean> getBlock_devs() {
        return block_devs;
    }

    public void setBlock_devs(List<BlockDevsBean> block_devs) {
        this.block_devs = block_devs;
    }

    public static class BlockDevsBean {
        /**
         * block_name : 地块1
         * devices : [{"id":1,"name":"设备1_1"},{"id":2,"name":"设备1_2"},{"id":6,"name":"1"}]
         * block_id : 1
         */

        private String block_name;
        private int block_id;
        private List<DevicesBean> devices;

        public String getBlock_name() {
            return block_name;
        }

        public void setBlock_name(String block_name) {
            this.block_name = block_name;
        }

        public int getBlock_id() {
            return block_id;
        }

        public void setBlock_id(int block_id) {
            this.block_id = block_id;
        }

        public List<DevicesBean> getDevices() {
            return devices;
        }

        public void setDevices(List<DevicesBean> devices) {
            this.devices = devices;
        }

        public static class DevicesBean implements Serializable{
            /**
             * id : 1
             * name : 设备1_1
             */

            private int id;
            private String name;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }
}
