package com.newenergy.ui.patrol.upload;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lzy.imagepicker.bean.ImageItem;
import com.newenergy.R;

import java.util.List;

/**
 * Created by ls on 17/9/17.
 */

public class PicPickerAdapter extends RecyclerView.Adapter<PicPickerAdapter.UriViewHolder> {

    private List<ImageItem> mImgs;

    public PicPickerAdapter(List<ImageItem> imgs) {
        mImgs = imgs;
    }

    void setData(List<ImageItem> uris) {
        mImgs = uris;
        notifyDataSetChanged();
    }


    @Override
    public UriViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UriViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_img_select, parent, false));
    }

    @Override
    public void onBindViewHolder(UriViewHolder holder, int position) {
        if (isAddBtn(position)) {
            holder.mImg.setImageResource(R.drawable.ic_camera);
        } else {
            Glide.with(holder.itemView.getContext()).load(mImgs.get(position).path)
                    .apply(new RequestOptions()
                            .centerCrop()
                    ).into(holder.mImg);
        }
    }

    private boolean isAddBtn(int position) {
        return mImgs.size() == 0 || mImgs.size() < PicSelectFragment.PARAM_MAX_COUNT && position == mImgs.size();
    }

    private OnRecyclerViewItemClickListener mListener;


    public interface OnRecyclerViewItemClickListener {
        void onItemClick(View view, int position, boolean isAdd);
    }

    public void setOnItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.mListener = listener;
    }

    @Override
    public int getItemCount() {
        return mImgs == null ? 1 : mImgs.size() == PicSelectFragment.PARAM_MAX_COUNT ? mImgs.size() : mImgs.size() + 1;
    }

    class UriViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView mImg;

        UriViewHolder(View contentView) {
            super(contentView);
            mImg = (ImageView) contentView.findViewById(R.id.img);
            mImg.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (isAddBtn(getLayoutPosition())) {
                //holder.mImg.setImageResource(R.drawable.ic_camera);
                if (mListener != null) mListener.onItemClick(view, getLayoutPosition(), true);

            } else {
                if (mListener != null) mListener.onItemClick(view, getLayoutPosition(), false);
            }
        }
    }
}
