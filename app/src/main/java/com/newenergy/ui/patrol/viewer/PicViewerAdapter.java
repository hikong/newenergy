package com.newenergy.ui.patrol.viewer;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.lzy.imagepicker.ImagePicker;
import com.newenergy.R;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.patrol.upload.GlideImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liushuai on 2017/9/25.
 */

public class PicViewerAdapter extends BaseQuickAdapter<PicBean.QuemaiimgBean, BaseViewHolder> {
    ArrayList<String> mSelImageList = new ArrayList<>(); //当前选择的所有图片

    public PicViewerAdapter(@Nullable List<PicBean.QuemaiimgBean> data) {
        super(R.layout.item_img_select, data);
        initImagePicker();
    }

    private void initImgs() {
        if (mData != null && !mData.isEmpty()) {
            mSelImageList.clear();
            for (PicBean.QuemaiimgBean quemaiimgBean : mData) {
                mSelImageList.add(String.format(RequestUrl.QUEMAIIMG, quemaiimgBean.getImgpath()));
            }
        }
    }

    private void initImagePicker() {
        ImagePicker imagePicker = ImagePicker.getInstance();
        imagePicker.setImageLoader(new GlideImageLoader());   //设置图片加载器
    }
    @Override
    public void setNewData(@Nullable List<PicBean.QuemaiimgBean> data) {
        super.setNewData(data);
        initImgs();
    }

    @Override
    protected void convert(final BaseViewHolder helper, PicBean.QuemaiimgBean item) {
        Glide.with(mContext)
                .load(String.format(RequestUrl.QUEMAIIMG, item.getImgpath()))
                .apply(new RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_img)
                )
                .into((ImageView) helper.getView(R.id.img));

        helper.getView(R.id.img).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentArgs args = new FragmentArgs();
                args.add(PicShowFragment.PARAM_IMGS, mSelImageList);
                args.add(PicShowFragment.PARAM_POSITION, helper.getAdapterPosition());
                args.add(FragmentContainerActivity.FRAGMENT_CONTAINER_LAYOUT_ID, R.layout.fragment_pic_show_activity_layout);
                FragmentContainerActivity.launch((BaseActivity)mContext, PicShowFragment.class, args);
            }
        });
    }

}
