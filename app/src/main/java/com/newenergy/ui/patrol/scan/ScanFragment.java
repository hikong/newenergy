package com.newenergy.ui.patrol.scan;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.VolleyError;
import com.google.zxing.Result;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.fragment.ABaseFragment;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.IResponseListener;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.StringParseNetwork;
import com.newenergy.ui.patrol.upload.PicSelectFragment;

import butterknife.BindView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by ls on 17/9/3.
 */

public class ScanFragment extends ABaseFragment implements ZXingScannerView.ResultHandler {
    @BindView(R.id.scanView)
    ZXingScannerView mScanView;

    private MaterialDialog mMaterialDialog;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle("扫一扫");
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_scan;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mMaterialDialog = new MaterialDialog.Builder(getActivity())
                .content("保存信息中")
                .progress(true, 0)
                .build();
    }

    @Override
    public void onDestroyView() {
        if (mMaterialDialog != null) {
            mMaterialDialog.dismiss();
            mMaterialDialog = null;
        }

        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        mScanView.setResultHandler(this);
        mScanView.startCamera();
        mScanView.setMaskColor(getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public void onPause() {
        super.onPause();
        mScanView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
//        Toast.makeText(getActivity(), "Contents = " + rawResult.getText() +
//                ", Format = " + rawResult.getBarcodeFormat().toString(), Toast.LENGTH_SHORT).show();
//
//        // Note:
//        // * Wait 2 seconds to resume the preview.
//        // * On older devices continuously stopping and resuming camera preview can result in freezing the app.
//        // * I don't know why this is the case but I don't have the time to figure out.
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mScanView.resumeCameraPreview(ScanFragment.this);
//            }
//        }, 2000);

        String result = rawResult.getText();
        ScanBean scanBean = JsonUtils.fromJson(result, ScanBean.class);
        if (scanBean != null) {
            final String deviceId = scanBean.getDevid();
            if (!TextUtils.isEmpty(deviceId)) {
                mMaterialDialog.show();
                Request r = RequestDefine.saveQueryMaintain(deviceId);
                CommonRequest<String> request = new CommonRequest<>(r, new StringParseNetwork());
                request.setResponseListener(new IResponseListener<String>() {
                    @Override
                    public void onResponse(int requestId, String response) {
                        if (getActivity() != null) {
                            mMaterialDialog.dismiss();
                            Toast.makeText(getActivity(), "保存成功", Toast.LENGTH_SHORT).show();
                            FragmentArgs args = new FragmentArgs();
                            args.add(PicSelectFragment.PARAM_QUERYID, deviceId);
                            FragmentContainerActivity.launch(getActivity(), PicSelectFragment.class, args);
                        }
                    }

                    @Override
                    public void onErrorResponse(int requestId, VolleyError error) {
                        mMaterialDialog.dismiss();
                        resumeCarema();
                    }
                });
                sendRequest(request);
            }
        } else {
            resumeCarema();

        }
    }

    private void resumeCarema() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mScanView.resumeCameraPreview(ScanFragment.this);
            }
        }, 2000);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.partol_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.patrol_device:
                //goToScanActivity();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}