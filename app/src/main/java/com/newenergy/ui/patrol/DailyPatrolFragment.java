package com.newenergy.ui.patrol;

import android.os.Bundle;

import com.newenergy.R;
import com.newenergy.ui.base.fragment.ABaseTabFragment;
import com.newenergy.ui.patrol.latest.PatrolNewListFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Liushuai on 2018/3/26.
 */

public class DailyPatrolFragment extends ABaseTabFragment {
    @Override
    public int inflateContentView() {
        return R.layout.fragment_manager;
    }

    @Override
    protected List<ABaseTabFragment.FragmentParam> initFramgentData() {
        ArrayList<ABaseTabFragment.FragmentParam> list = new ArrayList<>();
        Bundle args1 = new Bundle();
        args1.putString(PatrolNewListFragment.PARAM_TYPE, PatrolNewListFragment.TYPE_1);


        Bundle args2 = new Bundle();
        args2.putString(PatrolNewListFragment.PARAM_TYPE, PatrolNewListFragment.TYPE_2);

        Bundle args3 = new Bundle();
        args3.putString(PatrolNewListFragment.PARAM_TYPE, PatrolNewListFragment.TYPE_3);

        FragmentParam fragmentParam1 = new ABaseTabFragment.FragmentParam(getResources().getString(R.string.title_manager1), PatrolFragment.class.getName());
        fragmentParam1.setArgs(args1);

        FragmentParam fragmentParam2 = new ABaseTabFragment.FragmentParam(getResources().getString(R.string.title_manager2), PatrolFragment.class.getName());
        fragmentParam2.setArgs(args2);

        FragmentParam fragmentParam3 = new ABaseTabFragment.FragmentParam(getResources().getString(R.string.title_manager3), PatrolFragment.class.getName());
        fragmentParam3.setArgs(args3);

        list.add(fragmentParam1);
        list.add(fragmentParam2);
        list.add(fragmentParam3);
        return list;
    }
}