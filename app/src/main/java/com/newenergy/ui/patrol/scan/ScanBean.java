package com.newenergy.ui.patrol.scan;

/**
 * Created by ls on 17/9/24.
 */

public class ScanBean {
    private String devid;

    public String getDevid() {
        return devid;
    }

    public void setDevid(String devid) {
        this.devid = devid;
    }
}
