package com.newenergy.ui.patrol.viewer;

import com.newenergy.ui.base.bean.BaseBean;

import java.util.List;

/**
 * Created by liushuai on 2017/9/25.
 */

public class PicBean extends BaseBean{

    private List<QuemaiimgBean> quemaiimg;

    public List<QuemaiimgBean> getQuemaiimg() {
        return quemaiimg;
    }

    public void setQuemaiimg(List<QuemaiimgBean> quemaiimg) {
        this.quemaiimg = quemaiimg;
    }

    public static class QuemaiimgBean {
        /**
         * createtime : 2017-09-18 11:30:21
         * id : 5
         * imgpath : E:\image\0850d7d96efa4ee1b48dcb234978782f.png
         */

        private String createtime;
        private int id;
        private String imgpath;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImgpath() {
            return imgpath;
        }

        public void setImgpath(String imgpath) {
            this.imgpath = imgpath;
        }
    }
}
