package com.newenergy.ui.patrol.system;

import com.chad.library.adapter.base.entity.SectionEntity;

import java.util.List;

/**
 * Created by Liushuai on 2018/3/26.
 */

public class SystemSectionEntity extends SectionEntity<List<SystemDeviceBean.SysDevsBean.DevicesBean>> {
    private String mCount;
    public SystemSectionEntity(String header, String count) {
        super(true, header);
        mCount = count;
    }

    public SystemSectionEntity(List<SystemDeviceBean.SysDevsBean.DevicesBean> devicesBean) {
        super(devicesBean);
    }

    public String getCount() {
        return mCount;
    }

    public void setCount(String count) {
        mCount = count;
    }
}
