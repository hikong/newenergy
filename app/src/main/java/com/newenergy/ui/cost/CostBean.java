package com.newenergy.ui.cost;

import com.newenergy.ui.base.bean.BaseBean;

/**
 * Created by tongwenwen on 2018/3/5.
 */

public class CostBean extends BaseBean{


    /**
     * status : 1
     * costVals : {"electricVal":0,"waterVal":0,"coolVal":0,"profitVal":0}
     */

    private CostValsBean costVals;

    public CostValsBean getCostVals() {
        return costVals;
    }

    public void setCostVals(CostValsBean costVals) {
        this.costVals = costVals;
    }

    public static class CostValsBean {
        /**
         * electricVal : 0.0
         * waterVal : 0.0
         * coolVal : 0.0
         * profitVal : 0.0
         */

        private double electricVal;
        private double waterVal;
        private double coolVal;
        private double profitVal;

        public double getElectricVal() {
            return electricVal;
        }

        public void setElectricVal(double electricVal) {
            this.electricVal = electricVal;
        }

        public double getWaterVal() {
            return waterVal;
        }

        public void setWaterVal(double waterVal) {
            this.waterVal = waterVal;
        }

        public double getCoolVal() {
            return coolVal;
        }

        public void setCoolVal(double coolVal) {
            this.coolVal = coolVal;
        }

        public double getProfitVal() {
            return profitVal;
        }

        public void setProfitVal(double profitVal) {
            this.profitVal = profitVal;
        }
    }
}
