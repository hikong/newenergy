package com.newenergy.ui.cost;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.base.widge.DatePickView;

import butterknife.BindView;

/**
 * 成本计量
 */
public class CostCountFragment extends ABaseRequestFragment<CostBean> {

    @BindView(R.id.datePickView)
    DatePickView mDatePickView;
    @BindView(R.id.container)
    LinearLayout mContainer;
    @BindView(R.id.empty)
    View mEmptyView;
    @BindView(R.id.progress)
    View mProgress;

    @Override
    public int inflateContentView() {
        return R.layout.fragment_cost_count;
    }


    @Override
    public CostBean loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(CostBean response) {

    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDatePickView.setOnDateChangeListener(new DatePickView.DataChangeListener() {
            @Override
            public void onChange() {
                loadNetData(true);
            }
        });
    }

    @Override
    protected void beforeLoadData(boolean isNetLoad, boolean isRefresh) {
        //super.beforeLoadData(isNetLoad, isRefresh);
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    protected BaseVolleyRequest<CostBean> createNetRequest(boolean isRefresh) {
        Request r = RequestDefine.getCostCount(mDatePickView.getLeftTime(), mDatePickView.getRightTime());
        return new CommonRequest<>(r, new IParseNetwork<CostBean>() {
            @Override
            public CostBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, new TypeToken<CostBean>() {
                });
            }
        });
    }

    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, CostBean response) {
        super.onResponse(isNetResponse, isRefresh, response);
        if (response != null) {
            if (response.getCostVals() != null) {
                showEmpty(false);
                CostBean.CostValsBean costValsBean = response.getCostVals();
                ((TextView) (mContainer.getChildAt(0)).findViewById(R.id.title)).setText("累计电费" + costValsBean.getElectricVal());
                ((TextView) (mContainer.getChildAt(1)).findViewById(R.id.title)).setText("累计水费" + costValsBean.getWaterVal());
                ((TextView) (mContainer.getChildAt(2)).findViewById(R.id.title)).setText("累计制冷" + costValsBean.getCoolVal());
                ((TextView) (mContainer.getChildAt(03)).findViewById(R.id.title)).setText("盈利" + costValsBean.getProfitVal());
            } else {
                Toast.makeText(getContext(), response.getMsg(), Toast.LENGTH_SHORT).show();
                showEmpty(true);
            }
        }else {
            showEmpty(true);
        }
    }

    @Override
    public void onError(boolean isRefresh, VolleyError error) {
        super.onError(isRefresh, error);
        showEmpty(true);
    }

    private void showEmpty(boolean isEmpty) {
        mProgress.setVisibility(View.GONE);
        mContainer.setVisibility(isEmpty? View.GONE : View.VISIBLE);
        mEmptyView.setVisibility(isEmpty? View.VISIBLE : View.GONE);
    }
}
