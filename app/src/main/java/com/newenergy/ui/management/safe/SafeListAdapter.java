package com.newenergy.ui.management.safe;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.common.constant.Constant;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.web.WebFragment;

/**
 * Created by ls on 17/9/12.
 */

public class SafeListAdapter extends BaseQuickAdapter<SafeBean.PatrollistBean, BaseViewHolder> {
    public SafeListAdapter() {
        super(R.layout.item_manager_daily_list_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, final SafeBean.PatrollistBean item) {
        helper.setText(R.id.name, item.getUsername());
        helper.setText(R.id.manageName, String.format(mContext.getResources().getString(R.string.manager_name), item.getName()));
        helper.setText(R.id.time, item.getCreatetime());
        if (TextUtils.isEmpty(item.getMemo())) {
            helper.setVisible(R.id.rl_extra, false);
        } else {
            helper.setVisible(R.id.rl_extra, true);
            helper.setText(R.id.tv_extra, item.getMemo());
        }

        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = String.format(RequestUrl.VIEW_RPT, Constant.AQXC, item.getId(), "");
                WebFragment.Launch((Activity) mContext, url, mContext.getResources().getString(R.string.manager_safe));
            }
        });
    }
}
