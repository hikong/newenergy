package com.newenergy.ui.management.upload;

import com.newenergy.ui.base.bean.BaseBean;

import java.util.List;

/**
 * Created by tongwenwen on 2018/3/25.
 */

public class ManagerDataBean extends BaseBean{

    private List<RolesBean> roles;
    private List<GroupBean> group;

    public List<RolesBean> getRoles() {
        return roles;
    }

    public void setRoles(List<RolesBean> roles) {
        this.roles = roles;
    }

    public List<GroupBean> getGroup() {
        return group;
    }

    public void setGroup(List<GroupBean> group) {
        this.group = group;
    }

    public static class RolesBean {
        /**
         * pincode :
         * id : 1
         * isused : true
         * memo : 123456
         * name : 管理员
         * pid : 0
         */

        private String pincode;
        private int id;
        private boolean isused;
        private String memo;
        private String name;
        private int pid;

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public boolean isIsused() {
            return isused;
        }

        public void setIsused(boolean isused) {
            this.isused = isused;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getPid() {
            return pid;
        }

        public void setPid(int pid) {
            this.pid = pid;
        }
    }

    public static class GroupBean {
        /**
         * id : 42
         * simcode : wy
         * name : 物业
         * dicttype : manageoptimizetype
         * father_id : 41
         * realid : 1
         * listsn : 1
         * isuse : true
         */

        private int id;
        private String simcode;
        private String name;
        private String dicttype;
        private int father_id;
        private int realid;
        private int listsn;
        private boolean isuse;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getSimcode() {
            return simcode;
        }

        public void setSimcode(String simcode) {
            this.simcode = simcode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDicttype() {
            return dicttype;
        }

        public void setDicttype(String dicttype) {
            this.dicttype = dicttype;
        }

        public int getFather_id() {
            return father_id;
        }

        public void setFather_id(int father_id) {
            this.father_id = father_id;
        }

        public int getRealid() {
            return realid;
        }

        public void setRealid(int realid) {
            this.realid = realid;
        }

        public int getListsn() {
            return listsn;
        }

        public void setListsn(int listsn) {
            this.listsn = listsn;
        }

        public boolean isIsuse() {
            return isuse;
        }

        public void setIsuse(boolean isuse) {
            this.isuse = isuse;
        }
    }
}
