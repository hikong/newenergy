package com.newenergy.ui.management.train;

import com.newenergy.ui.base.bean.BaseListBean;

import java.util.List;

/**
 * Created by liushuai on 2017/9/21.
 */

public class TrainListBean extends BaseListBean {

    private List<TrainingBean> training;

    public List<TrainingBean> getTraining() {
        return training;
    }

    public void setTraining(List<TrainingBean> training) {
        this.training = training;
    }

    public static class TrainingBean {
        /**
         * content : 演练内容1
         * username : 张三
         * trainstatus_name : 未演练
         * name : 消防训练
         * trainstatus : 1
         * train_id : 1
         * traintime : 2017-08-31 10:17:16
         */

        private String content;
        private String username;
        private String trainstatus_name;
        private String name;
        private int trainstatus;
        private String train_id;
        private String traintime;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getTrainstatus_name() {
            return trainstatus_name;
        }

        public void setTrainstatus_name(String trainstatus_name) {
            this.trainstatus_name = trainstatus_name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getTrainstatus() {
            return trainstatus;
        }

        public void setTrainstatus(int trainstatus) {
            this.trainstatus = trainstatus;
        }

        public String getTrain_id() {
            return train_id;
        }

        public void setTrain_id(String train_id) {
            this.train_id = train_id;
        }

        public String getTraintime() {
            return traintime;
        }

        public void setTraintime(String traintime) {
            this.traintime = traintime;
        }
    }
}
