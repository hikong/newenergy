package com.newenergy.ui.management.safe;

import android.os.Bundle;

import com.newenergy.R;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.management.daily.DailyManagerUploadFragment;
import com.newenergy.ui.patrol.upload.FileUploadBean;
import com.newenergy.ui.patrol.upload.PicSelectFragment;

/**
 * Created by tongwenwen on 2018/3/14.
 */

public class SafeUploadFragment extends DailyManagerUploadFragment {

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle(getResources().getString(R.string.manager_daily));
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_daily_manager_pic_select;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected String getUploadUrl() {
        return String.format(RequestUrl.POST_UPLOAD, mId);
    }

    @Override
    protected void dealResponse(int requestId, FileUploadBean response) {
        super.dealResponse(requestId, response);
    }


}
