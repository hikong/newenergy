package com.newenergy.ui.management.train;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.common.utils.ViewUtils;
import com.newenergy.ui.base.activity.FragmentArgs;
import com.newenergy.ui.base.activity.FragmentContainerActivity;
import com.newenergy.ui.base.fragment.ABaseFragment;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.base.network.request.IResponseListener;
import com.newenergy.ui.base.network.request.MutipartRequestUpload;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.patrol.upload.FileUploadBean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by liushuai on 2017/9/20.
 */

public class TrainPdfUploadFragment extends ABaseFragment implements DialogSelectionListener {

    @BindView(R.id.add)
    ImageView mAdd;
    @BindView(R.id.pdfPreView)
    View mPreView;

    private FilePickerDialog mPickerDialog;
    private String mTrainId;
    private String mFilePath;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTrainId = getArguments().getString(TrainReportFragment.PARAM_TRAIN_ID);
        }
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_train_pdf_upload;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.FILE_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = new String[]{"pdf"};
        mPickerDialog = new FilePickerDialog(getActivity(), properties);
        mPickerDialog.setPositiveBtnName(getResources().getString(R.string.train_commit));
        mPickerDialog.setNegativeBtnName(getResources().getString(R.string.train_cancel));
    }

    @Override
    public void onDestroyView() {
        if (mPickerDialog != null) {
            mPickerDialog.dismiss();
            mPickerDialog = null;
        }
        super.onDestroyView();
    }

    @OnClick({R.id.add, R.id.upload, R.id.cancel, R.id.pdfViewer})
    void onClick(View v) {
        switch (v.getId()) {
            case R.id.add:
                showPDFPickDialog();
                break;
            case R.id.upload:
                uploadPdf();
                break;
            case R.id.cancel:
                clear();
                break;
            case R.id.pdfViewer:
                gotoPdfViewer();
                break;
        }

    }

    /**
     * pdf预览
     */
    private void gotoPdfViewer() {
        FragmentArgs args = new FragmentArgs();
        args.add(TrainReportFragment.PARAM_TRAIN_PATH, mFilePath);
        args.add(TrainReportViewerFragment.PARAM_REPORT_TYPE, TrainReportViewerFragment.PARAM_REPORT_TYPE_LOCAL);
        FragmentContainerActivity.launch(getActivity(), TrainReportViewerFragment.class, args);
    }

    /**
     * 删除所选pdf
     */
    private void clear() {
        mFilePath = "";
        ViewUtils.setVisible(mPreView, false);
        ViewUtils.setVisible(mAdd, true);
    }

    /**
     * 上传pdf
     */
    private void uploadPdf() {
        if (TextUtils.isEmpty(mTrainId) || TextUtils.isEmpty(mFilePath)) {
            return;
        }
        String url = String.format(RequestUrl.POST_TRAIN_REPORT, mTrainId);
        List<File> data = new ArrayList<>();
        data.add(new File(mFilePath));
        MutipartRequestUpload<FileUploadBean> requestUpload = new MutipartRequestUpload<>(url, data, new IParseNetwork<FileUploadBean>() {
            @Override
            public FileUploadBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, FileUploadBean.class);
            }
        });
        requestUpload.setResponseListener(new IResponseListener<FileUploadBean>() {
            @Override
            public void onResponse(int requestId, FileUploadBean response) {
                Toast.makeText(getContext(), "上传成功 filePath = " + response.getFilepath(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onErrorResponse(int requestId, VolleyError error) {

            }
        });
        sendRequest(requestUpload);
    }


    private void showPDFPickDialog() {
        if (mPickerDialog != null) {
            mPickerDialog.show();
            mPickerDialog.setDialogSelectionListener(this);
        }
    }


    @Override
    public void onSelectedFilePaths(String[] files) {
        if (files != null && files.length > 0) {
            mFilePath = files[0];
            Toast.makeText(getActivity(), mFilePath, Toast.LENGTH_SHORT).show();
            ViewUtils.setVisible(mAdd, false);
            ViewUtils.setVisible(mPreView, true);
        }
    }
}
