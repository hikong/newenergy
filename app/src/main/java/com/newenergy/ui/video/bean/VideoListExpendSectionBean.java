package com.newenergy.ui.video.bean;

import com.chad.library.adapter.base.entity.AbstractExpandableItem;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.newenergy.ui.video.list.VideoListChildAdapter;

/**
 * Created by tongwenwen on 2017/11/19.
 */

public class VideoListExpendSectionBean extends AbstractExpandableItem<VideoListBean.ControlUnitListBean.RegionListBean.CameraListBean> implements MultiItemEntity {

    private int level;

    private String name;

    public VideoListExpendSectionBean(int level, String name) {
        this.level = level;
        this.name = name;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int getLevel() {
        return level;
    }

    @Override
    public int getItemType() {
        if (level == 1) {
            return VideoListChildAdapter.TYPE_LEVEL_0;
        }else if (level == 2) {
            return VideoListChildAdapter.TYPE_LEVEL_1;
        }else {
            return VideoListChildAdapter.TYPE_LEVEL_2;
        }
    }

}
