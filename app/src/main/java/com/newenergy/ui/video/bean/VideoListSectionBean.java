package com.newenergy.ui.video.bean;

import com.chad.library.adapter.base.entity.SectionEntity;

import java.util.List;

/**
 * Created by liushuai on 2017/11/13.
 */

public class VideoListSectionBean extends SectionEntity<List<VideoListChildSectionBean>>{

    public VideoListSectionBean(boolean isHeader, String header) {
        super(isHeader, header);
    }

    public VideoListSectionBean(List<VideoListChildSectionBean> regionListBeen) {
        super(regionListBeen);
    }
}
