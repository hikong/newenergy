package com.newenergy.ui.video.detail;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;

import com.android.volley.VolleyError;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.BaseApplication;
import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.IResponseListener;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.base.network.task.VolleyManager;
import com.newenergy.ui.base.widge.MVideoView;
import com.newenergy.ui.video.bean.VideoDetailBean;

import butterknife.BindView;

/**
 * Created by tongwenwen on 2017/12/4.
 */

public class VideoDetailActivity extends BaseActivity implements MVideoView.Listener {
    public final static String PARAM_CAMERA_BEAN = "CameraBean";

    @BindView(R.id.videoView)
    MVideoView mMVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        BaseApplication.getInstance().initCore();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            mToolbar.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            setSupportActionBar(mToolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            }
        }

        Bundle args = getIntent().getExtras();
        if (args != null && !TextUtils.isEmpty(args.getString(PARAM_CAMERA_BEAN))) {
            final Request request = RequestDefine.getVideoRun(args.getString(PARAM_CAMERA_BEAN));
            CommonRequest<VideoDetailBean> r = new CommonRequest<>(request, new IParseNetwork<VideoDetailBean>() {
                @Override
                public VideoDetailBean parseNetworkResponse(String jsonStr) {
                    return JsonUtils.fromJson(jsonStr, VideoDetailBean.class);
                }
            });
            VolleyManager.addRequest(r);
//        String url = "rtsp://60.30.168.34:7554/livestream?userID=12345&puID="+ "22" +"&channelNo=0&streamtype=2&netType=0&streamEncoding=1&acsip=60.30.168.34&acsport=7002&acsusr=admin&acspwd=2015yujiapu&usrtye=0&ptzcontrol=1";
//        mMVideoView.prepare(url);
//        mMVideoView.start();

            r.setResponseListener(new IResponseListener<VideoDetailBean>() {
                @Override
                public void onResponse(int requestId, VideoDetailBean response) {
                    if (response != null && !TextUtils.isEmpty(response.getUrl())) {
                        mMVideoView.prepare(response.getUrl());
                        mMVideoView.start();
                    }
                }

                @Override
                public void onErrorResponse(int requestId, VolleyError error) {

                }
            });
        }

        mMVideoView.setListener(this);
        //全屏
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
        getWindow().setAttributes(params);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        Log.d(TAG, "con");
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == 1) {
            mMVideoView.setScreenOrientaion(2);
        }else {
            mMVideoView.setScreenOrientaion(1);
        }
    }

    @Override
    public int expend() {
        Configuration mConfiguration = this.getResources().getConfiguration(); //获取设置的配置信息
        int ori = mConfiguration.orientation; //获取屏幕方向
        if (ori == mConfiguration.ORIENTATION_LANDSCAPE) {
            //横屏
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//强制为竖屏
        } else if (ori == mConfiguration.ORIENTATION_PORTRAIT) {
            //竖屏
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//强制为横屏
        }
        return ori;
    }

    @Override
    public boolean onBackClick() {
        Configuration mConfiguration = this.getResources().getConfiguration(); //获取设置的配置信息
        if (mConfiguration.orientation== mConfiguration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//强制为竖屏
            mMVideoView.setScreenOrientaion(2);
            return false;
        }
        return super.onBackClick();
    }
}

