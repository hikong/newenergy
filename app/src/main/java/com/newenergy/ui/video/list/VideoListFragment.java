package com.newenergy.ui.video.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.video.bean.VideoListBean;
import com.newenergy.ui.video.bean.VideoListChildSectionBean;
import com.newenergy.ui.video.bean.VideoListExpendSectionBean;
import com.newenergy.ui.video.bean.VideoListSectionBean;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by liushuai on 2017/11/13.
 */

public class VideoListFragment extends ABaseRequestFragment<VideoListBean> {

    public final static String TAG = "VideoListFragment";
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private List<MultiItemEntity> mVideoListSectionBeans = new ArrayList<>();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    protected BaseVolleyRequest<VideoListBean> createNetRequest(boolean isRefresh) {
        Request request = RequestDefine.getVideoList();
        CommonRequest<VideoListBean> r = new CommonRequest<>(request, new IParseNetwork<VideoListBean>() {
            @Override
            public VideoListBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, VideoListBean.class);
            }
        });
        return r;
    }

    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, VideoListBean response) {
        super.onResponse(isNetResponse, isRefresh, response);
        if (response != null && response.getControlUnitList() != null) {
            List<VideoListBean.ControlUnitListBean> unitList = response.getControlUnitList();

            for (int i = 0; i < unitList.size(); i++) {
                VideoListBean.ControlUnitListBean unitListBean = unitList.get(i);
                VideoListExpendSectionBean sectionBean1 = new VideoListExpendSectionBean(unitListBean.getLevel(), unitListBean.getNodename());
                mVideoListSectionBeans.add(sectionBean1);

                List<VideoListBean.ControlUnitListBean.RegionListBean> regionList = unitListBean.getRegionList();
                if (regionList != null) {
                    for (VideoListBean.ControlUnitListBean.RegionListBean regionListBean : regionList) {
                        VideoListExpendSectionBean sectionBean2 = new VideoListExpendSectionBean(regionListBean.getLevel(), regionListBean.getNodename());
                        mVideoListSectionBeans.add(sectionBean2);
                        sectionBean2.setExpanded(true);//默认展开
                        for (VideoListBean.ControlUnitListBean.RegionListBean.CameraListBean cameraListBean : regionListBean.getCameraList()) {
                            //VideoListExpendSectionBean sectionBean3 = new VideoListExpendSectionBean(cameraListBean.getLevel(), cameraListBean.getNodename());
                            mVideoListSectionBeans.add(cameraListBean);
                            sectionBean2.addSubItem(cameraListBean);
                        }
                    }
                }
            }

            Log.d(TAG, mVideoListSectionBeans.size() + "");
            mRecyclerView.setAdapter(new VideoListChildAdapter(mVideoListSectionBeans));
        }
    }

    @Override
    public VideoListBean loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(VideoListBean response) {

    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_group_manager;
    }
}
