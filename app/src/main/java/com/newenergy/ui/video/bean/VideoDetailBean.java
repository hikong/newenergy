package com.newenergy.ui.video.bean;

import com.newenergy.ui.base.bean.BaseBean;

/**
 * Created by tongwenwen on 2017/12/5.
 */

public class VideoDetailBean extends BaseBean {

    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
