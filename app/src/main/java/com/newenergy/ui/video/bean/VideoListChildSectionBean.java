package com.newenergy.ui.video.bean;

import com.chad.library.adapter.base.entity.SectionEntity;

import java.util.List;

/**
 * Created by liushuai on 2017/11/13.
 */

public class VideoListChildSectionBean extends SectionEntity<VideoListBean.ControlUnitListBean.RegionListBean.CameraListBean>{

    public VideoListChildSectionBean(boolean isHeader, String header) {
        super(isHeader, header);
    }


    public VideoListChildSectionBean(VideoListBean.ControlUnitListBean.RegionListBean.CameraListBean cameraListBeen) {
        super(cameraListBeen);
    }
}
