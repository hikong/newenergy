package com.newenergy.ui.equitpment;

import android.os.Bundle;

import com.newenergy.R;
import com.newenergy.ui.base.fragment.ABaseTabFragment;
import com.newenergy.ui.patrol.deviceinfo.DeviceListFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ls on 17/9/22.
 */

public class EquipmentFragment extends ABaseTabFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionBarTitle(getString(R.string.equipment_title));
    }

    @Override
    protected List<ABaseTabFragment.FragmentParam> initFramgentData() {
        ArrayList<ABaseTabFragment.FragmentParam> list = new ArrayList<>();
        list.add(new FragmentParam(getResources().getString(R.string.equipment_warn), EquipmentWarnListFragment.class.getName()));
        list.add(new FragmentParam(getResources().getString(R.string.equipment_stand), DeviceListFragment.class.getName()));
        return list;
    }
}
