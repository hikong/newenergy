package com.newenergy.ui.equitpment;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.common.utils.CalendarUtil;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.ABaseRequestListFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestConstant;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

/**
 * Created by ls on 17/9/22.
 */

public class EquipmentWarnListFragment extends ABaseRequestListFragment<EquipmentItemBean.DevwarinfoBean, EquipmentItemBean, Object> {


    @Override
    protected BaseVolleyRequest<EquipmentItemBean> createNetRequest(boolean isRefresh) {
        String startTime = "2017-8-1 00:00:00";
        String endTime = CalendarUtil.getNowTime("yyyy-MM-dd HH:mm:ss");
        Request r = RequestDefine.getWarnList(startTime, endTime, getPageIndex());
        return new CommonRequest<>(r, new IParseNetwork<EquipmentItemBean>() {
            @Override
            public EquipmentItemBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, EquipmentItemBean.class);
            }
        });
    }

    @Override
    protected boolean checkHasMore(EquipmentItemBean response) {
        return response != null && response.getDevwarinfo() != null && response.getDevwarinfo().size() == RequestConstant.REQUEST_PAGE_SIZE;
    }

    @Override
    protected boolean checkValidResponse(EquipmentItemBean response) {
        return response != null && response.getDevwarinfo() != null;
    }

    @Override
    protected void updateAdapterData(BaseQuickAdapter<EquipmentItemBean.DevwarinfoBean, BaseViewHolder> adapter, EquipmentItemBean response, boolean isRefresh, boolean isNetResponse) {
        if (response != null && response.getDevwarinfo() != null) {
            if (isRefresh) {
                adapter.setNewData(response.getDevwarinfo());
            } else {
                adapter.addData(response.getDevwarinfo());
            }
        }
    }

    @Override
    protected BaseQuickAdapter<EquipmentItemBean.DevwarinfoBean, BaseViewHolder> createAdapter() {
        return new EquipmentListAdapter();
    }
}
