package com.newenergy.ui.equitpment.detail;

import com.newenergy.ui.base.bean.BaseListBean;

import java.util.List;

/**
 * Created by ls on 17/9/24.
 */

public class EquipmentDetailBean extends BaseListBean {


    private List<DevrecordsBean> devrecords;

    public List<DevrecordsBean> getDevrecords() {
        return devrecords;
    }

    public void setDevrecords(List<DevrecordsBean> devrecords) {
        this.devrecords = devrecords;
    }

    public static class DevrecordsBean {
        /**
         * createtime : 2017-09-10 14:52:35
         * id : 1
         * devname : 设备1_1
         * blockname : 地块1
         */

        private String createtime;
        private int id;
        private int status;
        private String devname;
        private String blockname;

        public String getCreatetime() {
            return createtime;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getDevname() {
            return devname;
        }

        public void setDevname(String devname) {
            this.devname = devname;
        }

        public String getBlockname() {
            return blockname;
        }

        public void setBlockname(String blockname) {
            this.blockname = blockname;
        }
    }
}
