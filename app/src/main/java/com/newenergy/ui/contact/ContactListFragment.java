package com.newenergy.ui.contact;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.ABaseRequestFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by ls on 17/9/5.
 */

public class ContactListFragment extends ABaseRequestFragment<ContactBean> {
    private static final int EXTERNAL_CALL_PERMISSION = 1;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setActionBarTitle(getResources().getString(R.string.title_contact));
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_contact;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public ContactBean loadLocal() {
        return null;
    }

    @Override
    public void onLocalResponse(ContactBean response) {

    }

    @Override
    public boolean isUIAdded() {
        return isAdded();
    }

    @Override
    protected BaseVolleyRequest<ContactBean> createNetRequest(boolean isRefresh) {
        Request r = RequestDefine.getContactInfo();
        return new CommonRequest<>(r, new IParseNetwork<ContactBean>() {
            @Override
            public ContactBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, ContactBean.class);
            }
        });
    }

    @Override
    protected void onResponse(boolean isNetResponse, boolean isRefresh, ContactBean response) {
        super.onResponse(isNetResponse, isRefresh, response);
        if (response != null && response.getGroup_contact() != null) {
            List<ContactSectionBean> data = new ArrayList<>();
            List<ContactBean.GroupContactBean> group_contact = response.getGroup_contact();

            for (ContactBean.GroupContactBean bean : group_contact) {
                List<ContactBean.GroupContactBean.ContactsBean> users = bean.getContacts();
                String count = "0";
                if (users != null) {
                    count = String.valueOf(users.size());

                }
                //添加header
                data.add(new ContactSectionBean(bean.getGroup_name(), count));
                if (users != null) {

                    for (int i = 0; i < users.size(); i++) {
                        ContactBean.GroupContactBean.ContactsBean user = users.get(i);
                        if (i == users.size() - 1) {
                            user.setHasDivider(false);
                        } else {
                            user.setHasDivider(true);
                        }
                        //添加child
                        data.add(new ContactSectionBean(user));
                    }
                }
            }

            ContactSectionAdapter adapter = new ContactSectionAdapter(data);
            adapter.setOnDialogClickListener(new ContactSectionAdapter.OnDialogClickListener() {
                @Override
                public boolean onItemClick(String phone) {
                    return callNum(phone);
                }
            });
            mRecyclerView.setAdapter(adapter);
        }
    }

    boolean callNum(String phone) {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CALL_PHONE}, EXTERNAL_CALL_PERMISSION);
            return false;
        } else {
            Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + phone));
            getContext().startActivity(intent);
            return true;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_CALL_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //goToScanActivity();
                    //goToPicSelect();
                } else {
                    Toast.makeText(getActivity(), "请开权限", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }

}
