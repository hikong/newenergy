package com.newenergy.ui.contact;

import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ls on 17/9/18.
 */

public class ContactSectionAdapter extends BaseSectionQuickAdapter<ContactSectionBean, BaseViewHolder> implements BaseQuickAdapter.OnItemChildClickListener {
    public ContactSectionAdapter(List<ContactSectionBean> data) {
        super(R.layout.item_contact_section_child, R.layout.item_patrol_section_head, data);
        setOnItemChildClickListener(this);
    }

    @Override
    protected void convertHead(BaseViewHolder helper, ContactSectionBean item) {
        helper.setText(R.id.title, item.header);
        helper.setText(R.id.count, item.getCount());
    }

    @Override
    protected void convert(BaseViewHolder helper, ContactSectionBean item) {
        helper.setText(R.id.name, item.t.getName());
        helper.setText(R.id.phone, item.t.getPhone());
        helper.setVisible(R.id.divider, item.t.isHasDivider());
        helper.addOnClickListener(R.id.root);
    }


    @Override
    public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
        TextView textView = (TextView) view.findViewById(R.id.phone);
        Log.d("Contact", position + "");
        final String phone = textView.getText().toString();
        final MaterialDialog dialog = new MaterialDialog.Builder(view.getContext())
                .customView(R.layout.dialog_contact, false)
                .negativeColor(view.getContext().getResources().getColor(R.color.colorBlack))
                .build();
        dialog.show();
        final View v = dialog.getCustomView();
        if (v != null) {
            ((TextView) v.findViewById(R.id.phone)).setText(phone);
            v.findViewById(R.id.positive).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            v.findViewById(R.id.negative).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Pattern p = Pattern.compile("\\d+?");
                    Matcher match = p.matcher(phone);
                    //正则验证输入的是否为数字
                    if (match.matches()) {
                        if (mListener != null) {
                            if (mListener.onItemClick(phone)) {
                                dialog.dismiss();
                            }
                        }
                    } else {
                        Toast.makeText(v.getContext(), "号码不对", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                }
            });
        }
    }


    private OnDialogClickListener mListener;


    public interface OnDialogClickListener {
        boolean onItemClick(String phone);
    }

    public void setOnDialogClickListener(OnDialogClickListener listener) {
        this.mListener = listener;
    }

}
