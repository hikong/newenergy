package com.newenergy.ui.customer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.newenergy.R;
import com.newenergy.common.utils.JsonUtils;
import com.newenergy.ui.base.fragment.ABaseRequestListFragment;
import com.newenergy.ui.base.network.request.BaseVolleyRequest;
import com.newenergy.ui.base.network.request.CommonRequest;
import com.newenergy.ui.base.network.request.RequestDefine;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;
import com.newenergy.ui.customer.bean.CustomerBean;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by liushuai on 2018/3/24.
 */

public class CustomerManagerListFragment extends ABaseRequestListFragment<CustomerBean.CustomerlistBean, CustomerBean, Object> {

    @BindView(R.id.editText)
    EditText mEditText;
    @BindView(R.id.recylcerView)
    View recyclerView;

    @Override
    public int inflateContentView() {
        return R.layout.fragment_customer_list;
    }

    @Override
    protected boolean checkHasMore(CustomerBean response) {
        return response != null && response.getCustomerlist() != null && response.getCustomerlist().size() == response.getPageinfo().getPageNumber();
    }

    @Override
    protected BaseVolleyRequest<CustomerBean> createNetRequest(boolean isRefresh) {
        Request r = RequestDefine.getCustomers(mEditText.getText().toString(), getPageIndex());

        return new CommonRequest<>(r, new IParseNetwork<CustomerBean>() {
            @Override
            public CustomerBean parseNetworkResponse(String jsonStr) {
                return JsonUtils.fromJson(jsonStr, CustomerBean.class);
            }
        });
    }

    @Override
    protected boolean checkValidResponse(CustomerBean response) {
        return response != null;
    }

    @Override
    protected void updateAdapterData(BaseQuickAdapter<CustomerBean.CustomerlistBean, BaseViewHolder> adapter, CustomerBean response, boolean isRefresh, boolean isNetResponse) {
        if (isRefresh) {
            adapter.setNewData(response.getCustomerlist());
        } else {
            adapter.addData(response.getCustomerlist());
        }
    }

    @Override
    protected BaseQuickAdapter<CustomerBean.CustomerlistBean, BaseViewHolder> createAdapter() {
        return new CustomerManagerListAdapter();
    }

    @OnClick({R.id.search, R.id.root})
    void click(View v) {
        switch (v.getId()) {
            case R.id.search:
                loadNetData(true);
            case R.id.root:
                hideInput(v);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    hideInput(v);
                }
                return false;
            }
        });

        mEditText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_DOWN == event.getAction()) {
                    mEditText.setCursorVisible(true);// 再次点击显示光标
                }
                return false;
            }
        });
    }


    @Override
    protected boolean forceShowContentView() {
        return true;
    }

    private void hideInput(View view) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        // 强制隐藏软键盘
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        //去除光标
        mEditText.setCursorVisible(false);
    }
}
