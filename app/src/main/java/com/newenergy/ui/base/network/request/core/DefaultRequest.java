package com.newenergy.ui.base.network.request.core;


import com.android.internal.http.multipart.Part;
import com.newenergy.ui.base.network.request.apachewrapper.FormPair;
import com.newenergy.ui.base.network.request.apachewrapper.Header;

import java.util.List;

/**
 * Created by lmhit on 16/7/19.
 */
public class DefaultRequest implements Request {

    private MethodType mMethod;
    private String mURL;
    private int mRequestTimeout;
    private List<Header> mHeaders;
    private List<Part> mBodyParts;
    private List<FormPair> mQueryParams;
    private String mStringEntity;

    public DefaultRequest(MethodType method, String uri, List<Header> headers, List<FormPair> params, List<Part> bodyParts, String stringEntity, int requestTimeout) {
        this.mMethod = method;
        this.mURL = uri;
        this.mHeaders = headers;
        this.mQueryParams = params;
        this.mBodyParts = bodyParts;
        this.mRequestTimeout = requestTimeout;
        this.mStringEntity = stringEntity;
    }

    @Override
    public String getUrl() {
        return mURL;
    }

    @Override
    public MethodType getMethod() {
        return mMethod;
    }

    @Override
    public List<Header> getHeaders() {
        return mHeaders;
    }

    @Override
    public List<Part> getBodyParts() {
        return mBodyParts;
    }

    @Override
    public int getRequestTimeout() {
        return mRequestTimeout;
    }

    //TODO  maybe get query param  from url
    @Override
    public String getStringEntity() {
        return mStringEntity;
    }

    @Override
    public List<FormPair> getQueryParams() {
        return mQueryParams;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(getUrl());

        sb.append("\t");
        sb.append(mMethod);
        sb.append("\tmHeaders:");
        if (!mHeaders.isEmpty()) {
            for (Header header : mHeaders) {
                sb.append("\t");
                sb.append(header.getName());
                sb.append(":");
                sb.append(header.getValue());
            }
        }
        if (HttpConstants.isNonEmpty(mQueryParams)) {
            sb.append("\tformParams:");
            for (FormPair param : mQueryParams) {
                sb.append("\t");
                sb.append(param.getName());
                sb.append(":");
                sb.append(param.getValue());
            }
        }

        return sb.toString();
    }
}
