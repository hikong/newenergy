package com.newenergy.ui.base.network.request;

import android.text.TextUtils;
import android.util.Log;

import com.newenergy.common.utils.PrHelper;
import com.newenergy.ui.base.network.request.apachewrapper.FormPair;
import com.newenergy.ui.base.network.request.apachewrapper.Header;
import com.newenergy.ui.base.network.request.core.MethodType;
import com.newenergy.ui.base.network.request.core.Request;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ls on 17/9/12.
 */

public class CommonRequest<T> extends BaseVolleyRequest<T> {
    private Request mRequest;

    public CommonRequest(Request r, IParseNetwork<T> iParseNetwork) {
        super(convertMethod(r.getMethod()), convertGetUrl(r.getUrl(), r.getQueryParams()));
        parseNetwork(iParseNetwork);
        mRequest = r;
    }

    public static int convertMethod(MethodType methodType) {
        if (methodType == MethodType.DELETE) {
            return com.android.volley.Request.Method.DELETE;
        } else if (methodType == MethodType.HEAD) {
            return com.android.volley.Request.Method.HEAD;
        } else if (methodType == MethodType.OPTIONS) {
            return com.android.volley.Request.Method.OPTIONS;
        } else if (methodType == MethodType.PATCH) {
            return com.android.volley.Request.Method.PATCH;
        } else if (methodType == MethodType.POST) {
            return com.android.volley.Request.Method.POST;
        } else if (methodType == MethodType.PUT) {
            return com.android.volley.Request.Method.PUT;
        } else if (methodType == MethodType.TRACE) {
            return com.android.volley.Request.Method.TRACE;
        } else {
            return com.android.volley.Request.Method.GET;
        }
    }

    public static String convertGetUrl(String url, List<FormPair> queryParams) {
        if (TextUtils.isEmpty(url)) {
            return null;
        }
        if (queryParams == null || queryParams.isEmpty()) {
            return url;
        }
        StringBuilder sBuilder = new StringBuilder(url);
        boolean isFirstParam = true;
        for (FormPair FormPair : queryParams) {
            if (FormPair != null && !TextUtils.isEmpty(FormPair.getName())) {
                if (isFirstParam) {
                    sBuilder.append("?");
                }
                sBuilder.append(FormPair.getName()).append("=").append(FormPair.getValue()).append("&");
                isFirstParam = false;
            }
        }
        int lastAndIndex = sBuilder.lastIndexOf("&");
        Log.d("Request url", sBuilder.toString());
        if (lastAndIndex == sBuilder.length() - 1) {
            return sBuilder.substring(0, sBuilder.length() - 1);
        }
        return sBuilder.toString();
    }

    @Override
    protected Map<String, String> getHeaderMap() {
        Map<String, String> headers = super.getHeaderMap();
        List<Header> headerList = mRequest.getHeaders();
        if (headerList != null && !headerList.isEmpty()) {
            for (Header header : headerList) {
                if (header != null) {
                    headers.put(header.getName(), header.getValue());
                }
            }
        }
        return headers;
    }
}

