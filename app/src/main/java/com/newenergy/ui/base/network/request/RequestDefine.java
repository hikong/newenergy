package com.newenergy.ui.base.network.request;


import android.text.TextUtils;

import com.newenergy.common.utils.PrHelper;
import com.newenergy.ui.base.network.RequestUrl;
import com.newenergy.ui.base.network.request.apachewrapper.FormPair;
import com.newenergy.ui.base.network.request.apachewrapper.Header;
import com.newenergy.ui.base.network.request.core.Request;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ls on 16/8/10.
 */
public class RequestDefine {

    public static Request loginRequest(String account, String password) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("account", account));
        pairs.add(new FormPair("password", password));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.LOGIN_URL, pairs);
    }


    public static Request getMainListRequest(String url, String startTime, String endTime, String curpage) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("startTime", startTime));
        pairs.add(new FormPair("endTime", endTime));
        pairs.add(new FormPair("curpage", curpage));
        pairs.add(new FormPair("pagenum", String.valueOf(RequestConstant.REQUEST_PAGE_SIZE)));
        return BaseRequestGenerator.requestGetTypeWithParams(url, pairs);
    }

    public static Request getDeviceInfo() {
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.DEVICEINFO, null);
    }

    public static Request getSystemDeviceInfo() {
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_SYSTEM_DEVICEINFO, null);
    }

    //扫描二维码保存巡查维护信息
    public static Request saveQueryMaintain(String deviceId) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("device_id", deviceId));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.SAVE_QUERY_MAINTAIN, pairs);
    }


    /**
     * 联系方式
     *
     * @return
     */
    public static Request getContactInfo() {
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.CONTACT_INFO, null);
    }

    /**
     * 群组信息
     *
     * @return
     */
    public static Request getGroupUser() {
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GROUP_USER, null);
    }

    /**
     * 群组移动
     *
     * @return
     */
    public static Request modifyUserGroup(String userId, String groupId) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("user_id", userId));
        pairs.add(new FormPair("role_id", groupId));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GROUP_USER_NODIFY, pairs);
    }


    /**
     * 查询培训演练
     *
     * @param startTime
     * @param endTime
     * @param curpage
     * @return
     */
    public static Request getTraining(String startTime, String endTime, String curpage) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("startTime", startTime));
        pairs.add(new FormPair("endTime", endTime));
        pairs.add(new FormPair("curpage", curpage));
        pairs.add(new FormPair("pagenum", String.valueOf(RequestConstant.REQUEST_PAGE_SIZE)));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_TRAINING, pairs);
    }

    /**
     * 查询报警列表
     *
     * @param startTime
     * @param endTime
     * @param curpage
     * @return
     */
    public static Request getWarnList(String startTime, String endTime, String curpage) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("startTime", startTime));
        pairs.add(new FormPair("endTime", endTime));
        pairs.add(new FormPair("curpage", curpage));
        pairs.add(new FormPair("pagenum", String.valueOf(RequestConstant.REQUEST_PAGE_SIZE)));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_WARN_INFO, pairs);
    }

    /**
     * 根据设备id查询设备维护记录
     *
     * @param startTime
     * @param endTime
     * @param curpage
     * @return
     */
    public static Request getDevRec(String startTime, String endTime, String curpage, String deviceId) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("startTime", startTime));
        pairs.add(new FormPair("endTime", endTime));
        pairs.add(new FormPair("curpage", curpage));
        pairs.add(new FormPair("device_id", deviceId));
        pairs.add(new FormPair("pagenum", String.valueOf(RequestConstant.REQUEST_PAGE_SIZE)));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_DEVREC, pairs);
    }

    /**
     * 查询培训演练报告路径
     *
     * @param trainId
     * @return
     */
    public static Request getTrainReport(String trainId) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("train_id", trainId));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_TRAIN_REPORT, pairs);
    }

    /**
     * 巡查图片查看
     *
     * @param querymaintainId
     * @return
     */
    public static Request getQueryMaintainImg(String url, String querymaintainId) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("querymaintain_id", querymaintainId));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_QUERY_MAINTAIN_IMG, pairs);
    }

    /**
     * 巡查图片查看
     *
     * @return
     */
    public static Request getUserInfo() {
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_USERINFO, null);
    }

    /**
     * 监控列表
     *
     * @return
     */
    public static Request getVideoList() {
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_VIDEO_LIST, null);
    }

    /**
     * 监控播放
     *
     * @return
     */
    public static Request getVideoRun(String id) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("cameraID", id));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_VIDEO_RUN, pairs);
    }

    /**
     * 获取地块数据
     *
     * @return
     */
    public static Request getColdBlock() {
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_COLD_BLOCK, null);

    }

    public static Request getBlcokColdInfo(String type, String startTime, String endTime, String blockIds, String blockNames) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("startTime", startTime));
        pairs.add(new FormPair("endTime", endTime));
        pairs.add(new FormPair("blockIds", blockIds));
        pairs.add(new FormPair("blockNames", blockNames));
        return BaseRequestGenerator.requestGetTypeWithParams(String.format(RequestUrl.GET_COLD_BLOCK_INFO, type), pairs);

    }

    public static Request getElecData(String startTime, String endTime) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("startTime", startTime));
        pairs.add(new FormPair("endTime", endTime));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_ELEC_DATA, pairs);
    }

    /**
     * 成本计量
     */
    public static Request getCostCount(String startTime, String endTime) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("startTime", startTime));
        pairs.add(new FormPair("endTime", endTime));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_COST_COUNT, pairs);
    }

    /**
     * 日报管理
     */
    public static Request getDailyManager(String startTime, String endTime, String curpage) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("startTime", startTime));
        pairs.add(new FormPair("endTime", endTime));
        pairs.add(new FormPair("curpage", curpage));
        pairs.add(new FormPair("pagenum", String.valueOf(RequestConstant.REQUEST_PAGE_SIZE)));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_DAILY_MANAGER, pairs);
    }

    /**
     * 安全巡查
     */
    public static Request getPatrolList(String startTime, String endTime, String curpage) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("startTime", startTime));
        pairs.add(new FormPair("endTime", endTime));
        pairs.add(new FormPair("curpage", curpage));
        pairs.add(new FormPair("pagenum", String.valueOf(RequestConstant.REQUEST_PAGE_SIZE)));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_PATROL_LIST, pairs);
    }


    /**
     * 客户管理
     */

    public static Request getCustomers(String customername, String curpage) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        if (!TextUtils.isEmpty(customername)) {
            pairs.add(new FormPair("customername", customername));
        }
        pairs.add(new FormPair("curpage", curpage));
        pairs.add(new FormPair("pagenum", String.valueOf(RequestConstant.REQUEST_PAGE_SIZE)));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_CUSTOMERS, pairs);
    }

    /**
     * 客户信息
     */

    public static Request getCustomersInfo(String id) {
        List<FormPair> pairs = new ArrayList<FormPair>();
        pairs.add(new FormPair("customer_id", id));
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_CUSTOMERS_INFO, pairs);
    }

    /**
     * 获取提示人员和所属分组
     */

    public static Request getManagerDatas() {
        return BaseRequestGenerator.requestGetTypeWithParams(RequestUrl.GET_MANAGER_DATAS, null);
    }

    /**
     * 获取提示人员和所属分组
     */

    public static Request postManagerInfo(String url) {
        return BaseRequestGenerator.requestGetTypeWithParams(url, null);
    }

}
