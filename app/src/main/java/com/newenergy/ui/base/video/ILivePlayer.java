package com.newenergy.ui.base.video;

import android.app.Activity;

/**
 * Created by liushuai on 2017/11/13.
 */

public interface ILivePlayer {

    void prepare(String url);
    void start();
    void stop();
    void release();
}
