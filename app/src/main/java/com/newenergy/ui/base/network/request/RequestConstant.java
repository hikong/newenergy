package com.newenergy.ui.base.network.request;

/**
 * Created by lmhit on 16/7/26.
 */
public class RequestConstant {
    public final static int REQUEST_PAGE_SIZE = 10;

    public final static String RESPONSE_OK = "1";
    public final static String RESPONSE_ERROR = "-1";
    public final static String RESPONSE_SESSTION_TIMEOUT = "-2";

}
