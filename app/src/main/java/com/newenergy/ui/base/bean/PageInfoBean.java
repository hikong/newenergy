package com.newenergy.ui.base.bean;

/**
 * Created by liushuai on 2017/9/13.
 */

public class PageInfoBean {
    /**
     * totalNumber : 2
     * currentPage : 1
     * totalPage : 1
     * pageNumber : 20
     * dbIndex : 0
     * dbNumber : 20
     */

    private int totalNumber;
    private int currentPage;
    private int totalPage;
    private int pageNumber;
    private int dbIndex;
    private int dbNumber;

    public int getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(int totalNumber) {
        this.totalNumber = totalNumber;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getDbIndex() {
        return dbIndex;
    }

    public void setDbIndex(int dbIndex) {
        this.dbIndex = dbIndex;
    }

    public int getDbNumber() {
        return dbNumber;
    }

    public void setDbNumber(int dbNumber) {
        this.dbNumber = dbNumber;
    }
}