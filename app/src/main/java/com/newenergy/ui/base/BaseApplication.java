package com.newenergy.ui.base;

import android.app.Application;

import com.hik.mcrsdk.MCRSDK;
import com.hik.mcrsdk.rtsp.RtspClient;
import com.newenergy.ui.base.network.task.VolleyManager;
import com.crashlytics.android.Crashlytics;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import okhttp3.OkHttpClient;

/**
 * Created by liushuai on 2017/8/30.
 */

public class BaseApplication extends Application {

    private static BaseApplication mApplication;
    private boolean mCoreInit;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        mApplication = this;
        init();
    }

    private void init() {
        VolleyManager.init(this, 4);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10000L, TimeUnit.MILLISECONDS)
                .readTimeout(10000L, TimeUnit.MILLISECONDS)
                //其他配置
                .build();

        OkHttpUtils.initClient(okHttpClient);

    }

    public static BaseApplication getInstance() {
        return mApplication;
    }

    //只初始化一次。
    public void initCore() {
        if (!mCoreInit) {
            System.loadLibrary("SystemTransform");
            MCRSDK.init();
            RtspClient.initLib();
            MCRSDK.setPrint(1, null);
            mCoreInit = true;
        }
    }
}
