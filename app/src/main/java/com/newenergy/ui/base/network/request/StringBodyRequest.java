package com.newenergy.ui.base.network.request;

import android.text.TextUtils;

import com.android.volley.AuthFailureError;


public class StringBodyRequest<T> extends BaseVolleyRequest<T> {
    private String mStringBody;

    public StringBodyRequest(String url) {
        super(url);
    }

    public StringBodyRequest(int method, String url) {
        super(method, url);
    }

    public StringBodyRequest<T> stringBody(String stringBody) {
        mStringBody = stringBody;
        return this;
    }

    @Override
    public final byte[] getBody() throws AuthFailureError {
        if (TextUtils.isEmpty(mStringBody)) {
            return null;
        }
        try {
            return mStringBody.getBytes(getParamsEncoding());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getBodyContentType() {
        return "text/plain; charset=" + getParamsEncoding();
    }

}
