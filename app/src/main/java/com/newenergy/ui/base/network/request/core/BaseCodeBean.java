package com.newenergy.ui.base.network.request.core;


/**
 * Created by lijie on 16/9/7.
 * 只有一个字段为code的bean
 */
public class BaseCodeBean  {

    private String code = "0";

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
