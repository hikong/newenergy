package com.newenergy.ui.base.network.request.apachewrapper;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lijie on 16/12/28.
 * 封装{@link org.apache.http.client.utils.URLEncodedUtils}
 */

public class URLEncodedUtils {

    public static String format(List<? extends FormPair> parameters, String encoding) {
        List<NameValuePair> params = new ArrayList<>();
        for (FormPair pair : parameters) {
            params.add(pair.getRealValue());
        }
        return org.apache.http.client.utils.URLEncodedUtils.format(params, encoding);
    }

}
