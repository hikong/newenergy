package com.newenergy.ui.base.network.request.parser;

import com.newenergy.common.utils.JsonUtils;

/**
 * Created by heq on 16/11/2.
 */

public class JsonParseNetwork<T> implements IParseNetwork<T> {
    private Class<T> mClazz;

    public JsonParseNetwork(Class<T> clazz) {
        mClazz = clazz;
    }

    @Override
    public T parseNetworkResponse(String jsonStr) {
        return JsonUtils.fromJson(jsonStr, mClazz);
    }
}
