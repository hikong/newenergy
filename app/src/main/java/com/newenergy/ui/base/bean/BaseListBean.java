package com.newenergy.ui.base.bean;

/**
 * Created by liushuai on 2017/9/13.
 */

public class BaseListBean extends BaseBean {
    private PageInfoBean pageinfo;

    public PageInfoBean getPageinfo() {
        return pageinfo;
    }

    public void setPageinfo(PageInfoBean pageinfo) {
        this.pageinfo = pageinfo;
    }
}
