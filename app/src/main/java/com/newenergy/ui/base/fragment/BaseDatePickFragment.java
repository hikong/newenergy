package com.newenergy.ui.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.newenergy.R;
import com.newenergy.ui.base.widge.DatePickView;

import butterknife.BindView;

/**
 * Created by ls on 17/9/22.
 */

public abstract class BaseDatePickFragment<T, D> extends ABaseRequestListFragment<T, D, Object> {


    @BindView(R.id.datePickView)
    DatePickView mDatePickView;

    protected boolean mFromDataChange;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDatePickView().setOnDateChangeListener(new DatePickView.DataChangeListener() {
            @Override
            public void onChange() {
                mFromDataChange = true;
                loadNetData(true);
                mFromDataChange = false;
            }
        });
    }

    @Override
    protected boolean shouldShowProgressBeforeLoad() {
        return mFromDataChange || super.shouldShowProgressBeforeLoad();
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_date_pick_list;
    }

    @Override
    public void setContentView(ViewGroup view) {
        super.setContentView(view);
    }

    public DatePickView getDatePickView() {
        return mDatePickView;
    }

    @Override
    protected boolean forceShowContentView() {
        return true;
    }
}
