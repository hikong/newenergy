package com.newenergy.ui.base.network.request.core;

import android.util.Log;


import com.android.internal.http.multipart.Part;
import com.newenergy.ui.base.network.request.apachewrapper.FormPair;
import com.newenergy.ui.base.network.request.apachewrapper.Header;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lmhit on 16/7/19.
 */
public abstract class RequestBuilderBase<T extends RequestBuilderBase<T>> {

    static final String TAG = RequestBuilderBase.class.getName();

    protected MethodType mMethod;
    protected String mRUL;
    protected List<Header> mHeaders;
    protected List<Part> mBodyParts;
    protected List<FormPair> mQueryParams;
    protected int mRequestTimeout;
    protected String mStringEngity = null;


    protected RequestBuilderBase(MethodType method) {
        this.mMethod = method;
        this.mHeaders = new ArrayList<Header>();
    }

    protected RequestBuilderBase(Request prototype) {
        this.mMethod = prototype.getMethod();
        this.mRUL = prototype.getUrl();
        this.mHeaders = new ArrayList<Header>();
        this.mHeaders.addAll(prototype.getHeaders());
        if (HttpConstants.isNonEmpty(prototype.getBodyParts())) {
            this.mBodyParts = new ArrayList<>(prototype.getBodyParts());
        }
        this.mRequestTimeout = prototype.getRequestTimeout();
        this.mStringEngity = prototype.getStringEntity();
    }

    private T asDerivedType() {
        return (T) this;
    }

    public T setUrl(String url) {
        this.mRUL = url;
        return asDerivedType();
    }

    public T addHeader(String name, String value) {
        if (value == null) {
            Log.i(TAG, "Value was null, set to \"\"");
            value = "";
        }
        this.mHeaders.add(new Header(name, value));
        return asDerivedType();
    }

    public T setHeaders(List<Header> headers) {
        if (headers == null)
            this.mHeaders.clear();
        else
            this.mHeaders = headers;
        return asDerivedType();
    }

    public void resetMultipartData() {
        this.mBodyParts = null;
    }


    private void resetBody() {

        resetMultipartData();
    }


    public T addQueryParam(String name, String value) {
        if (mQueryParams == null)
            mQueryParams = new ArrayList<>(1);
        mQueryParams.add(new FormPair(name, value));
        return asDerivedType();
    }

    public T addQueryParams(List<FormPair> params) {
        if (mQueryParams == null)
            mQueryParams = params;
        else
            mQueryParams.addAll(params);
        return asDerivedType();
    }


    public T setQueryParams(List<FormPair> params) {
        mQueryParams = params;
        return asDerivedType();
    }

    public T addBodyPart(Part bodyPart) {
        if (this.mBodyParts == null)
            this.mBodyParts = new ArrayList<>();
        this.mBodyParts.add(bodyPart);
        return asDerivedType();
    }

    public T setBodyParts(List<Part> bodyParts) {
        this.mBodyParts = new ArrayList<>(bodyParts);
        return asDerivedType();
    }

    public T setRequestTimeout(int requestTimeout) {
        this.mRequestTimeout = requestTimeout;
        return asDerivedType();
    }

    public T setMethod(MethodType method) {
        this.mMethod = method;
        return asDerivedType();
    }

    public T setStringEntity(String entity) {
        this.mStringEngity = entity;
        return asDerivedType();
    }


    private RequestBuilderBase<?> executeSignatureCalculator() {

        RequestBuilder rb = new RequestBuilder(this.mMethod);
        if (this.mHeaders != null)
            rb.mHeaders.addAll(this.mHeaders);
        if (this.mBodyParts != null)
            rb.setBodyParts(this.mBodyParts);
        rb.mQueryParams = this.mQueryParams;
        rb.mRUL = this.mRUL;
        rb.mRequestTimeout = this.mRequestTimeout;
        rb.mStringEngity = this.mStringEngity;
        return rb;
    }

    public Request build() {
        RequestBuilderBase<?> rb = executeSignatureCalculator();
        String finalUri = rb.mRUL;

        return new DefaultRequest(rb.mMethod, finalUri, rb.mHeaders, rb.mQueryParams, rb.mBodyParts, rb.mStringEngity, rb.mRequestTimeout
        );
    }
}
