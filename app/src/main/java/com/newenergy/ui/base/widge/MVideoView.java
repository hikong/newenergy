package com.newenergy.ui.base.widge;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.newenergy.R;
import com.newenergy.common.threadpool.NRThreadPool;
import com.newenergy.common.utils.SystemUtils;
import com.newenergy.ui.base.BaseApplication;
import com.newenergy.ui.base.video.ConstantLive;
import com.newenergy.ui.base.video.ILivePlayer;
import com.newenergy.ui.base.video.LiveCallBack;
import com.newenergy.ui.base.video.LiveControl;

/**
 * Created by liushuai on 2017/11/13.
 */

public class MVideoView extends RelativeLayout implements ILivePlayer, LiveCallBack, View.OnClickListener {

    public final static String TAG = "MVideoView";
    private LiveControl mLiveControl;
    private SurfaceView mSurfaceView;
    private ImageView mExpendBtn;
    private ProgressBar mProgressBar;
    private Handler mMessageHandler = new MyHandler();
    private int mScreenWidth = SystemUtils.getScreenWidth();

    private int mScreenOrientaion = 2;

    public MVideoView(Context context) {
        super(context);
        init(context);
    }

    public MVideoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context c) {
        mLiveControl = new LiveControl();
        mLiveControl.setLiveCallBack(this);
        View v = LayoutInflater.from(c).inflate(R.layout.widge_video, this);
        mSurfaceView = (SurfaceView) v.findViewById(R.id.surfaceView);
        mExpendBtn = (ImageView) v.findViewById(R.id.expend);
        mExpendBtn.setOnClickListener(this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mProgressBar = (ProgressBar) findViewById(R.id.liveProgressBar);
    }

    @Override
    public void prepare(String url) {
        mLiveControl.setLiveParams(url);
    }

    @Override
    public void start() {
        NRThreadPool.runInBackground(new Runnable() {
            @Override
            public void run() {
                if (mSurfaceView != null) {
                    mLiveControl.startLive(mSurfaceView);
                    mProgressBar.setVisibility(VISIBLE);
                }
            }
        });
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = SystemUtils.getScreenWidth();
        if (mScreenOrientaion == 2) {
            int height = (int) (width * 0.6);
            setMeasuredDimension(width, height);
            super.onMeasure(MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY));
            //super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } else {
            setMeasuredDimension(MeasureSpec.makeMeasureSpec(SystemUtils.getScreenHeight(), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(SystemUtils.getScreenWidth(), MeasureSpec.EXACTLY));
            super.onMeasure(MeasureSpec.makeMeasureSpec(SystemUtils.getScreenHeight(), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(mScreenWidth, MeasureSpec.EXACTLY));

        }
    }

    @Override
    public void stop() {

    }

    @Override
    public void release() {

    }

    @Override
    public void onMessageCallback(int message) {
        if (null != mMessageHandler) {
            Message msg = Message.obtain();
            msg.arg1 = message;
            mMessageHandler.sendMessage(msg);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.expend:
                if (mListener != null) {
                    mScreenOrientaion = mListener.expend();
                    invalidate();
                }
                break;
        }
    }

    public void setScreenOrientaion(int screenOrientaion) {
        mScreenOrientaion = screenOrientaion;
        invalidate();
    }

    @Nullable
    @Override
    protected Parcelable onSaveInstanceState() {

        Bundle bundle = new Bundle();
        bundle.putInt("orientation", mScreenOrientaion);
        bundle.putParcelable("data", super.onSaveInstanceState());
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Bundle bundle = (Bundle) state;
        mScreenOrientaion = bundle.getInt("orientation");
        super.onRestoreInstanceState(bundle.getParcelable("data"));
    }

    private Listener mListener;

    public void setListener(Listener listener) {
        if (listener != null) {
            mListener = listener;
        }
    }

    public interface Listener {
        int expend();
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mListener != null) {
            mListener = null;
        }
        super.onDetachedFromWindow();
    }

    /**
     * 消息类
     *
     * @author huangweifeng
     * @Data 2013-10-23
     */
    @SuppressLint("HandlerLeak")
    private final class MyHandler extends Handler {
        public void handleMessage(Message msg) {
            switch (msg.arg1) {
                case ConstantLive.RTSP_SUCCESS:
                    //UIUtil.showToast(LiveActivity.this, "启动取流成功");
                    break;

                case ConstantLive.STOP_SUCCESS:
//                    UIUtil.showToast(LiveActivity.this, "停止成功");
                    break;

                case ConstantLive.START_OPEN_FAILED:
//                    UIUtil.showToast(LiveActivity.this, "开启播放库失败");
                    if (null != mProgressBar) {
                        mProgressBar.setVisibility(View.GONE);
                    }
                    break;

                case ConstantLive.PLAY_DISPLAY_SUCCESS:
//                    UIUtil.showToast(LiveActivity.this, "播放成功");
                    if (null != mProgressBar) {
                        mProgressBar.setVisibility(View.GONE);
                    }
                    break;

                case ConstantLive.RTSP_FAIL:
//                    UIUtil.showToast(LiveActivity.this, "RTSP链接失败");
                    if (null != mProgressBar) {
                        mProgressBar.setVisibility(View.GONE);
                    }
                    if (null != mLiveControl) {
                        mLiveControl.stop();
                    }
                    break;

                case ConstantLive.GET_OSD_TIME_FAIL:
//                    UIUtil.showToast(LiveActivity.this, "获取OSD时间失败");
                    break;

                case ConstantLive.SD_CARD_UN_USEABLE:
//                    UIUtil.showToast(LiveActivity.this, "SD卡不可用");
                    break;

                case ConstantLive.SD_CARD_SIZE_NOT_ENOUGH:
//                    UIUtil.showToast(LiveActivity.this, "SD卡空间不足");
                    break;
                case ConstantLive.CAPTURE_FAILED_NPLAY_STATE:
//                    UIUtil.showToast(LiveActivity.this, "非播放状态不能抓拍");
                    break;
            }
        }
    }
}
