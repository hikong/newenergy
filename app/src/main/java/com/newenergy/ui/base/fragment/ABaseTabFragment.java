package com.newenergy.ui.base.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.newenergy.R;
import com.newenergy.ui.patrol.TabEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * Created by tongwenwen on 17/9/22.
 */

public abstract class ABaseTabFragment extends ABaseFragment {
    @BindView(R.id.tabLayout)
    protected View mTab;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    private List<FragmentParam> mParams;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private String[] mTitles;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mParams = initFramgentData();
    }

    @Override
    public int inflateContentView() {
        return R.layout.fragment_base_tab;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mParams != null && !mParams.isEmpty()) {
            mTitles = new String[mParams.size()];

            for (int i = 0; i < mParams.size(); i++) {
                FragmentParam param = mParams.get(i);
                mFragments.add(Fragment.instantiate(getContext(), param.getFragmentName(), param.getArgs()));
                mTitles[i] = param.getTitle();
            }
        }

        if (mTab instanceof SegmentTabLayout) {
            final SegmentTabLayout tabLayout = (SegmentTabLayout) mTab;

            tabLayout.setTabData(mTitles);
            tabLayout.setOnTabSelectListener(new OnTabSelectListener() {
                @Override
                public void onTabSelect(int position) {
                    //tabSelected(position);
                    if (mViewPager != null) {
                        mViewPager.setCurrentItem(position, true);
                    }
                }

                @Override
                public void onTabReselect(int position) {

                }
            });

            //mTabLayout.setCurrentTab(0);

            mViewPager.setOffscreenPageLimit(3);
            mViewPager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (tabLayout != null) {
                        tabLayout.setCurrentTab(position);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }else if (mTab instanceof CommonTabLayout) {
            final CommonTabLayout tabLayout = (CommonTabLayout) mTab;
            ArrayList<CustomTabEntity> tabEntities = new ArrayList<>();
            for (int i = 0; i < mTitles.length; i++) {
                tabEntities.add(new TabEntity(mTitles[i], 0, 0));
            }
            tabLayout.setTabData(tabEntities);
            tabLayout.setOnTabSelectListener(new OnTabSelectListener() {
                @Override
                public void onTabSelect(int position) {
                    //tabSelected(position);
                    if (mViewPager != null) {
                        mViewPager.setCurrentItem(position, true);
                    }
                }

                @Override
                public void onTabReselect(int position) {

                }
            });

            //mTabLayout.setCurrentTab(0);

            mViewPager.setOffscreenPageLimit(3);
            mViewPager.setAdapter(new MyPagerAdapter(getChildFragmentManager()));

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    if (tabLayout != null) {
                        tabLayout.setCurrentTab(position);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
        }
    }

    abstract protected List<FragmentParam> initFramgentData();

    public static class FragmentParam {
        String mTitle;
        String mFragmentName;
        private Bundle mArgs;

        public FragmentParam(String title, String fragmentName) {
            mTitle = title;
            mFragmentName = fragmentName;
        }

        public String getTitle() {
            return mTitle;
        }

        public void setTitle(String title) {
            mTitle = title;
        }

        public String getFragmentName() {
            return mFragmentName;
        }

        public void setFragmentName(String fragmentName) {
            mFragmentName = fragmentName;
        }

        public Bundle getArgs() {
            return mArgs;
        }

        public void setArgs(Bundle args) {
            mArgs = args;
        }
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mParams.get(position).getTitle();
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }
    }

}
