package com.newenergy.ui.base.widge;

import android.app.DatePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.newenergy.R;
import com.newenergy.common.utils.CalendarUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ls on 17/9/12.
 */

public class DatePickView extends RelativeLayout implements View.OnClickListener {

    private View mLeftView;
    private View mRightView;

    private Date mLeftDate;
    private Date mRightDate;

    private final static long ONE_SECOND = 1000;
    private final static long ONE_DAY = 24 * 3600 * ONE_SECOND;

    private SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private String mLeftTime;
    private String mRightTime;

    public DatePickView(Context context) {
        super(context);
        init();
    }

    public DatePickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DataChangeListener mDataChangeListener;

    public interface DataChangeListener {
        void onChange();
    }

    public void setOnDateChangeListener(DataChangeListener listener) {
        mDataChangeListener = listener;
    }

    private void init() {
        inflate(getContext(), R.layout.widge_date_pick, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mLeftView = findViewById(R.id.leftView);
        mRightView = findViewById(R.id.rightView);
        long currentTime = System.currentTimeMillis();
        mRightDate = CalendarUtil.getDate(currentTime, "yyyy-MM-dd");
        mLeftDate = CalendarUtil.getDate(currentTime - ONE_DAY, "yyyy-MM-dd");

        setDate(mLeftView, mLeftDate);
        setDate(mRightView, mRightDate);

        setListener(mLeftView);
        setListener(mRightView);
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mDataChangeListener != null) {
            mDataChangeListener = null;
        }
        super.onDetachedFromWindow();
    }

    private void setDate(View v, Date date) {
        String year = String.valueOf(CalendarUtil.getYearByDate(date));
        String day = CalendarUtil.getDayTimeByDate(date);
        TextView tvYear = (TextView) v.findViewById(R.id.year);
        tvYear.setText(year);
        TextView tvDay = (TextView) v.findViewById(R.id.day);
        tvDay.setText(day);

        //中间两个按钮
        if ((mRightDate.getTime() - mLeftDate.getTime()) <= 0) {
            setArrowClickable((ImageView) mLeftView.findViewById(R.id.arrow_right), false);
            setArrowClickable((ImageView) mRightView.findViewById(R.id.arrow_left), false);
        } else {
            setArrowClickable((ImageView) mLeftView.findViewById(R.id.arrow_right), true);
            setArrowClickable((ImageView) mRightView.findViewById(R.id.arrow_left), true);
        }

        //左侧按钮
        setArrowClickable((ImageView) mLeftView.findViewById(R.id.arrow_left), true);

        //右侧按钮
        if (CalendarUtil.equals2Date(mRightDate, new Date())) {
            setArrowClickable((ImageView) mRightView.findViewById(R.id.arrow_right), false);
        } else {
            setArrowClickable((ImageView) mRightView.findViewById(R.id.arrow_right), true);
        }

        if (v == mLeftView) {
            mLeftTime = mDateFormat.format(date);
        } else if (v == mRightView) {
            mRightTime = mDateFormat.format(new Date(date.getTime() + ONE_DAY - 1000));
        }

        if (mDataChangeListener != null) {
            mDataChangeListener.onChange();
        }
    }

    private void setArrowClickable(ImageView v, boolean clickable) {
        v.setClickable(clickable);
        v.setImageLevel(clickable ? 1 : 0);
    }

    private void setListener(View v) {
        v.findViewById(R.id.arrow_left).setOnClickListener(this);
        v.findViewById(R.id.arrow_right).setOnClickListener(this);
        v.findViewById(R.id.dateContainer).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.arrow_left:
                if (((View) view.getParent()).getId() == R.id.leftView) {
                    mLeftDate = new Date(mLeftDate.getTime() - ONE_DAY);
                    setDate(mLeftView, mLeftDate);
                } else {
                    mRightDate = new Date(mRightDate.getTime() - ONE_DAY);
                    setDate(mRightView, mRightDate);
                }
                break;
            case R.id.arrow_right:
                if (((View) view.getParent()).getId() == R.id.leftView) {
                    mLeftDate = new Date(mLeftDate.getTime() + ONE_DAY);
                    setDate(mLeftView, mLeftDate);
                } else {
                    mRightDate = new Date(mRightDate.getTime() + ONE_DAY);
                    setDate(mRightView, mRightDate);
                }
                break;
            case R.id.dateContainer:
                if (((View) view.getParent()).getId() == R.id.leftView) {
                    showDateDialog(view.getContext(), mLeftDate, mLeftView);
                } else {
                    showDateDialog(view.getContext(), mRightDate, mRightView);
                }
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }

    private void showDateDialog(Context c, Date d, final View v) {
        DatePickerDialog dialog = new DatePickerDialog(c, 0, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                //Date d = new Date(year, month, dayOfMonth);
                if (v.getId() == R.id.leftView) {
                    mLeftDate = CalendarUtil.getDate(year, month, dayOfMonth, "yyyy-MM-dd");
                    setDate(mLeftView, mLeftDate);
                } else {
                    mRightDate = CalendarUtil.getDate(year, month, dayOfMonth, "yyyy-MM-dd");
                    setDate(mRightView, mRightDate);
                }
            }
        }, CalendarUtil.getYearByDate(d), CalendarUtil.getMonthByDate(d), CalendarUtil.getDayByDate(d));//后边三个参数为显示dialog时默认的日期，月份从0开始，0-11对应1-12个月


        if (v.getId() == R.id.rightView) {
            dialog.getDatePicker().setMinDate(mLeftDate.getTime());//右侧设置最小值
            dialog.getDatePicker().setMaxDate(System.currentTimeMillis());//右侧最大值为今天
        } else {
            dialog.getDatePicker().setMaxDate(mRightDate.getTime());//左测最大值为右侧值。
        }
        dialog.show();
    }


    /**
     * 对外提供接口
     */

    public String getLeftTime() {
        return mLeftTime;
    }

    public String getRightTime() {
        return mRightTime;
    }

    public void setDayTimeInterval(int dayTimeInterval) {
        mLeftDate = CalendarUtil.getDate(mRightDate.getTime() - dayTimeInterval * ONE_DAY, "yyyy-MM-dd");
        setDate(mLeftView, mLeftDate);

    }
}
