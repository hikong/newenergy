package com.newenergy.ui.base.widge;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

/**
 * 本类是对PagerAdapter的扩展,将ListAdapter的特性适用到PagerAdapter上<br/>
 * 使用本类时 使用方法和listview 使用 ListAdapter 方法一致
 *
 * @author wjying
 */
public abstract class ViewPagerAdapter extends PagerAdapter {

    private boolean mDataSetChanged;

    @Override
    public void notifyDataSetChanged() {
        mDataSetChanged = true;
        super.notifyDataSetChanged();
        mDataSetChanged = false;
    }

    @Override
    public abstract int getCount();

    public int getRealCount() {
        return getCount();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {

    }


    protected void setItemPositionNone(boolean yes) {
        mDataSetChanged = yes;
    }

    @Override
    public int getItemPosition(Object object) {
        if (mDataSetChanged) {
            return POSITION_NONE;
        }
        return super.getItemPosition(object);
    }

    @Override
    public final boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public final void startUpdate(ViewGroup container) {
        super.startUpdate(container);
//        mUpdate = false;
    }


    @Override
    public final void finishUpdate(ViewGroup container) {
        super.finishUpdate(container);

    }

    @Override
    public final Object instantiateItem(ViewGroup container, int position) {
        View view = null;

        view = getView(position, view, container);

        container.addView(view);
        return view;
    }

    @Override
    public final void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;

        container.removeView(view);

    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    public abstract View getView(int position, View convertView, ViewGroup parent);

    public int getViewTypeCount() {
        return 1;
    }


    public boolean isEmpty() {
        return getCount() == 0;
    }


}
