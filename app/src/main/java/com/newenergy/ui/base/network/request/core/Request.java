package com.newenergy.ui.base.network.request.core;


import com.android.internal.http.multipart.Part;
import com.newenergy.ui.base.network.request.apachewrapper.FormPair;
import com.newenergy.ui.base.network.request.apachewrapper.Header;

import java.util.List;

/**
 * Created by lmhit on 16/7/19.
 */
public interface Request {

    MethodType getMethod();

    String getUrl();

    List<Header> getHeaders();

    List<Part> getBodyParts();

    List<FormPair> getQueryParams();

    int getRequestTimeout();

    //add for support postStringEntity
    String getStringEntity();


}
