package com.newenergy.ui.base.widge;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

public class HackyViewPager extends ViewPager {

    private float mStartDragX;
    private float mStartDragY;
    private OnSwipeOutListener mOnSwipeOutListener;
    private OnScrollChangedListener mOnScrollChangedListener;
    private int mTouchSlop;
    private boolean needScroll = true;


    public HackyViewPager(Context context) {
        super(context);
        init(context);
    }

    public HackyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        final ViewConfiguration configuration = ViewConfiguration.get(context);
        mTouchSlop = configuration.getScaledTouchSlop();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!needScroll) {
            return false;
        }

        try {
            if (mOnSwipeOutListener != null) {
                float x = ev.getX();
                float y = ev.getY();
                switch (ev.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        mStartDragX = x;
                        mStartDragY = y;
                        break;
                    case MotionEvent.ACTION_MOVE:
                        float offsetX = Math.abs(x - mStartDragX);
                        float offsetY = Math.abs(y - mStartDragY);

                        if (offsetX > mTouchSlop && offsetX * 0.5f > offsetY) { //说明有明显的横向滑动
//                            if (mStartDragX < x && getCurrentItem() == 0) {//右滑动
//                                mOnSwipeOutListener.onSwipeOutAtStart();
//                            } else
                            if (mStartDragX > x && getCurrentItem() == getAdapter().getCount() - 1) {//左滑动
                                mOnSwipeOutListener.onSwipeOutAtEnd();
                            }
                        }
                        break;
                }
            }
            return super.onInterceptTouchEvent(ev);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (mOnScrollChangedListener != null) {
            mOnScrollChangedListener.onScrollChanged(this, l, t, oldl, oldt);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    public void setOnScrollChangedListener(OnScrollChangedListener onScrollChangedListener) {
        mOnScrollChangedListener = onScrollChangedListener;
    }

    public void setOnSwipeOutListener(OnSwipeOutListener listener) {
        mOnSwipeOutListener = listener;
    }

    public void setNeedScroll(boolean needScroll) {
        this.needScroll = needScroll;
    }

    public interface OnScrollChangedListener {
        void onScrollChanged(ViewPager viewPager, int l, int t, int oldl, int oldt);
    }

    public interface OnSwipeOutListener {
        void onSwipeOutAtEnd();
    }


}