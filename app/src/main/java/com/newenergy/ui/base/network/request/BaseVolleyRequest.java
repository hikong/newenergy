package com.newenergy.ui.base.network.request;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.newenergy.common.utils.PrHelper;
import com.newenergy.common.utils.string.StringUtils;
import com.newenergy.ui.base.network.request.parser.IParseNetwork;

import org.apache.http.protocol.HTTP;

import java.util.HashMap;
import java.util.Map;

public class BaseVolleyRequest<T> extends Request<T> {
    protected String TAG = getClass().getSimpleName();
    private IResponseListener<T> mResponseListener;
    private IRequestListener mRequestListener;
    private IDataHandler<T> mDataHandler;
    private int mRequestId;
    private IParseNetwork<T> mParseNetwork;
    private Priority mPriority = Priority.NORMAL;
    /**
     * 是否htmldecode处理返回的json串,默认是处理
     */
    private boolean mShouldHtmlDecodeResult = true;
    private Map<String, String> mHeaders;

    public BaseVolleyRequest(String url) {
        this(Method.GET, url);
    }

    public BaseVolleyRequest(int method, String url) {
        super(method, url, null);
        setShouldCache(false);
    }

    public BaseVolleyRequest<T> priority(Priority priority) {
        mPriority = priority;
        return this;
    }


    public BaseVolleyRequest<T> parseNetwork(IParseNetwork<T> parseNetwork) {
        mParseNetwork = parseNetwork;
        return this;
    }

    public BaseVolleyRequest<T> requestId(int requestId) {
        mRequestId = requestId;
        return this;
    }

    @Override
    public BaseVolleyRequest<?> setTag(Object tag) {
         super.setTag(tag);
        return this;
    }

    public BaseVolleyRequest<T> shouldHtmlDecodeResult(boolean bool) {
        mShouldHtmlDecodeResult = bool;
        return this;
    }

    public BaseVolleyRequest<T> setResponseListener(IResponseListener<T> responseListener) {
        mResponseListener = responseListener;
        return this;
    }

    public BaseVolleyRequest<T> setDataHandler(IDataHandler<T> dataHandler) {
        mDataHandler = dataHandler;
        return this;
    }

    public BaseVolleyRequest<T> setRequestListener(IRequestListener requestListener) {
        mRequestListener = requestListener;
        return this;
    }

    public int getRequestId() {
        return mRequestId;
    }


    @Override
    public final Map<String, String> getHeaders() throws AuthFailureError {
        if (mHeaders == null) {
            mHeaders = new HashMap<>();
            if (isDoGZIPForBody()) {
                mHeaders.put("Content-Encoding", "gzip");
            }
            Map<String, String> headers = getHeaderMap();
            if (headers != null && !headers.isEmpty()) {
                mHeaders.putAll(headers);
            }
        }
        return mHeaders;
    }

    protected Map<String, String> getHeaderMap() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Cookie", PrHelper.getPrSession());
        return headers;
    }

    @Override
    protected void deliverResponse(T response) {
        if (mResponseListener != null) {
            mResponseListener.onResponse(mRequestId, response);
        }
    }

    @Override
    public void deliverError(VolleyError error) {
        Log.e(TAG, "deliverError:" + getUrl() + " ;Error:" + error + ";StatusCode:" + ((error == null || error.networkResponse == null) ? "" : error.networkResponse.statusCode));
        if (mResponseListener != null) {
            mResponseListener.onErrorResponse(mRequestId, error);
        }
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers, HTTP.UTF_8));
            if (mShouldHtmlDecodeResult) {
                json = StringUtils.htmlDecoder(json);
            }
            Log.d("Request jsonData", json);
            for (Map.Entry<String, String> entry : response.headers.entrySet()){
                Log.d("entry key = ", entry.getKey() + "  entry value = " + entry.getValue());
            }

            T data = parseNetworkResponse(json);
            if (mDataHandler != null) {
                data = mDataHandler.processData(data);
            }
            return Response.success(data, HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }

    @Deprecated
    //use mParseNetwork
    protected T parseNetworkResponse(String jsonStr) {
        if (mParseNetwork != null) {
            return mParseNetwork.parseNetworkResponse(jsonStr);
        }
        return null;
    }

    @Override
    public void cancel() {
        Log.d(TAG, "cancel request:" + getUrl());
        mParseNetwork = null;
        mResponseListener = null;
        mDataHandler = null;
        setTag(null);
        if (mRequestListener != null) {
            mRequestListener.onCancel(mRequestId);
        }
        mRequestListener = null;
        super.cancel();
    }

    @Override
    public Priority getPriority() {
        return mPriority;
    }

    /**
     * 是否对body进行gzip压缩
     *
     * @return
     */
    public boolean isDoGZIPForBody() {
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sBuilder = new StringBuilder();
        sBuilder.append(isCanceled() ? " [X] " : " ")
                .append(getPriority()).append(" ").append(getUrl());
        return sBuilder.toString();
    }


    public interface IDataHandler<T> {

        /**
         * 数据处理，该方法运行在异步线程中
         *
         * @param data 原始数据
         */
        T processData(T data);
    }

}
