package com.newenergy.ui.base.event;

/**
 * Created by ls on 2018/3/4.
 */

public class MessageEvent {
    String type;

    public MessageEvent(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
