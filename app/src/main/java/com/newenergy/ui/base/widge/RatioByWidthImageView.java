package com.newenergy.ui.base.widge;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.newenergy.R;

/**
 * Created by liushuai on 2017/8/29.
 */

public class RatioByWidthImageView extends AppCompatImageView{

    private float mRadio = 1.0f;

    public RatioByWidthImageView(Context context) {
        super(context);
    }

    public RatioByWidthImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomView);
        //第一个参数为属性集合里面的属性，R文件名称：R.styleable+属性集合名称+下划线+属性名称
        //第二个参数为，如果没有设置这个属性，则设置的默认的值
        mRadio = a.getFloat(R.styleable.CustomView_ratio, 1.0f);
        a.recycle();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = (int) (width * mRadio);
        setMeasuredDimension(width, height);
//        int childWidth = getMeasuredWidth();//得到宽度
//        heightMeasureSpec = widthMeasureSpec = MeasureSpec.makeMeasureSpec(childWidth, MeasureSpec.EXACTLY);
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
