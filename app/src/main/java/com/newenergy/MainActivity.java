package com.newenergy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabWidget;
import android.widget.TextView;

import com.newenergy.ui.base.activity.BaseActivity;
import com.newenergy.ui.base.widge.FragmentTabHost;
import com.newenergy.ui.main.HomeFragment;
import com.newenergy.ui.pc.PersonCenterFragment;

import butterknife.BindView;

public class MainActivity extends BaseActivity {

    @BindView(android.R.id.tabcontent)
    FrameLayout tabcontent;
    @BindView(android.R.id.tabs)
    TabWidget tabs;
    @BindView(R.id.navigation_section)
    FragmentTabHost tabHost;

    private final static String TAG_HOME = "home";
    private final static String TAG_PC = "pc";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LayoutInflater inflater = LayoutInflater.from(this);
        tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        addTab(tabHost, inflater, TAG_HOME, HomeFragment.class,
                R.drawable.nav_main_selector);
        addTab(tabHost, inflater, TAG_PC, PersonCenterFragment.class,
                R.drawable.nav_pc_selector);

        //tabs.setDividerDrawable(getResources().getDrawable(R.color.colorBlack2));
    }

    private void addTab(FragmentTabHost tabHost, LayoutInflater inflater, String tag, Class clss,
                        int icon) {
        LinearLayout indicator = (LinearLayout) inflater.inflate(R.layout.tab_navigation, null);
        ImageView imag = (ImageView) indicator.findViewById(R.id.nav_icon);
        imag.setImageResource(icon);
        tabHost.addTab(tabHost.newTabSpec(tag).setIndicator(indicator), clss, null);

    }
}
